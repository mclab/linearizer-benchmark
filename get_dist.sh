#! /usr/bin/env sh

cd /root/dist
wget http://ftp.gnu.org/gnu/glpk/glpk-4.44.tar.gz
tar xf glpk-4.44.tar.gz
cd glpk-4.44
cp /root/dist/qks/glpapi19.c src/glpapi19.c
cp /root/dist/qks/glpk.h include/glpk.h
./configure && make install
ldconfig

cd /root/dist
git clone https://github.com/ivmai/cudd.git cudd-2.4.2
cd cudd-2.4.2
git checkout cudd-2.4.2
sed -i '69s/^#//' Makefile
make

cd /root/dist
git clone https://bitbucket.org/mclab/lpsolver.git
cd lpsolver
git checkout tags/v1.1.0
./bootstrap && ./configure && make install

cd /root/dist
git clone https://bitbucket.org/mclab/linearizer.git
cd linearizer
git checkout tags/v1.0.1
./bootstrap && ./configure && make install

ldconfig

cd /root/dist
git clone https://bitbucket.org/mclab/lin4qks.git
cd lin4qks
git checkout tags/v0.0.6
make install
cd /root

cd /root/dist/qks
cp qks /usr/local/bin
cp qks.release /usr/local/bin

cd /root/dist/replicate4qks
make install
