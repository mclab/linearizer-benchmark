set terminal jpeg
set output "ctrl_reg.strong_ctrl.gnuplot.jpg"
unset key
set xlabel "x_x1"
set xrange [-3.454000:3.454000]
set ylabel "x_x2"
set yrange [-4.000000:4.000000]
#set grid
set object rectangle from -0.107937 , 0.000000 to -0.080953 , 0.031250 fs solid 0.3 noborder fc rgb "#79B1F3"
set object rectangle from -0.080953 , -0.125000 to 0.026984 , -0.093750 fs solid 0.3 noborder fc rgb "#79B1F3"
set object rectangle from 0.026984 , -0.156250 to 0.053969 , -0.093750 fs solid 0.3 noborder fc rgb "#79B1F3"
set object rectangle from 0.053969 , -0.187500 to 0.080953 , -0.125000 fs solid 0.3 noborder fc rgb "#79B1F3"
set object rectangle from 0.080953 , -0.218750 to 0.107938 , -0.125000 fs solid 0.3 noborder fc rgb "#79B1F3" 
set object rectangle from -0.107937 , 0.031250 to -0.080953 , 0.062500 fs solid 0.3 noborder fc rgb "#EE79F3"
set object rectangle from -0.080953 , -0.093750 to 0.026984 , -0.031250 fs solid 0.3 noborder fc rgb "#EE79F3"
set object rectangle from 0.026984 , -0.093750 to 0.053969 , -0.062500 fs solid 0.3 noborder fc rgb "#EE79F3"
set object rectangle from 0.053969 , -0.125000 to 0.107938 , -0.062500 fs solid 0.3 noborder fc rgb "#EE79F3" 
set object rectangle from -0.080953 , -0.031250 to -0.026984 , 0.062500 fs solid 0.3 noborder fc rgb "#79F3CA"
set object rectangle from -0.026984 , -0.031250 to 0.026984 , 0.031250 fs solid 0.3 noborder fc rgb "#79F3CA"
set object rectangle from 0.026984 , -0.062500 to 0.080953 , 0.031250 fs solid 0.3 noborder fc rgb "#79F3CA" 
set object rectangle from -0.107937 , 0.062500 to -0.053969 , 0.125000 fs solid 0.3 noborder fc rgb "#F3798D"
set object rectangle from -0.053969 , 0.062500 to -0.026984 , 0.093750 fs solid 0.3 noborder fc rgb "#F3798D"
set object rectangle from -0.026984 , 0.031250 to 0.080953 , 0.093750 fs solid 0.3 noborder fc rgb "#F3798D"
set object rectangle from 0.080953 , -0.062500 to 0.107938 , -0.031250 fs solid 0.3 noborder fc rgb "#F3798D" 
set object rectangle from -0.107937 , 0.125000 to -0.080953 , 0.218750 fs solid 0.3 noborder fc rgb "#AC79F3"
set object rectangle from -0.080953 , 0.125000 to -0.053969 , 0.187500 fs solid 0.3 noborder fc rgb "#AC79F3"
set object rectangle from -0.053969 , 0.093750 to -0.026984 , 0.156250 fs solid 0.3 noborder fc rgb "#AC79F3"
set object rectangle from -0.026984 , 0.093750 to 0.080953 , 0.125000 fs solid 0.3 noborder fc rgb "#AC79F3"
set object rectangle from 0.080953 , -0.031250 to 0.107938 , 0.000000 fs solid 0.3 noborder fc rgb "#AC79F3" 
plot -4.000000, 4.000000
