#include "ctrl.h"
#include <math.h>

int K_entries(unsigned char *, unsigned);

void DA(unsigned char *to_be_conv, double *conv)
{
  int tmp;
  tmp = 0;
  tmp += to_be_conv[0] << 0;
  tmp += to_be_conv[1] << 1;
  conv[0] = -1 + tmp;
}

void AD(double *to_be_conv, unsigned char *conv)
{
  unsigned tmp;
  tmp = (unsigned) floor((to_be_conv[0] - -3.454000)/0.026984);
  conv[0] = (tmp&(1 << 0)) >> 0;
  conv[1] = (tmp&(1 << 1)) >> 1;
  conv[2] = (tmp&(1 << 2)) >> 2;
  conv[3] = (tmp&(1 << 3)) >> 3;
  conv[4] = (tmp&(1 << 4)) >> 4;
  conv[5] = (tmp&(1 << 5)) >> 5;
  conv[6] = (tmp&(1 << 6)) >> 6;
  conv[7] = (tmp&(1 << 7)) >> 7;
  tmp = (unsigned) floor((to_be_conv[1] - -4.000000)/0.031250);
  conv[8] = (tmp&(1 << 0)) >> 0;
  conv[9] = (tmp&(1 << 1)) >> 1;
  conv[10] = (tmp&(1 << 2)) >> 2;
  conv[11] = (tmp&(1 << 3)) >> 3;
  conv[12] = (tmp&(1 << 4)) >> 4;
  conv[13] = (tmp&(1 << 5)) >> 5;
  conv[14] = (tmp&(1 << 6)) >> 6;
  conv[15] = (tmp&(1 << 7)) >> 7;
}

int K_with_AD_DA(double *input, double *output)
{
  static unsigned char conv[DIM_X_VECTOR];
  static unsigned char answer[DIM_A_VECTOR];
  int res;
  AD(input, conv);
  res = K(conv, answer);
  DA(answer, output);
  return res;
}

int exists_K_with_AD(double *input)
{
  static unsigned char conv[DIM_X_VECTOR];
  AD(input, conv);
  return K_exists(conv);
}

void DA_expl(unsigned char *to_be_conv, int *w_u)
{
  int tmp;
  tmp = 0;
  tmp += to_be_conv[0] << 0;
  tmp += to_be_conv[1] << 1;
  *w_u = -1 + tmp;
}

void DA_expl_w_u(unsigned char *to_be_conv, int *w_u)
{
  unsigned tmp = 0;
  tmp += to_be_conv[0] << 0;
  tmp += to_be_conv[1] << 1;
  *w_u = -1 + tmp;
}

void AD_expl(double x_x1, double x_x2, unsigned char *conv)
{
  unsigned tmp;
  tmp = (unsigned) floor((x_x1 - -3.454000)/0.026984);
  conv[0] = (tmp&(1 << 0)) >> 0;
  conv[1] = (tmp&(1 << 1)) >> 1;
  conv[2] = (tmp&(1 << 2)) >> 2;
  conv[3] = (tmp&(1 << 3)) >> 3;
  conv[4] = (tmp&(1 << 4)) >> 4;
  conv[5] = (tmp&(1 << 5)) >> 5;
  conv[6] = (tmp&(1 << 6)) >> 6;
  conv[7] = (tmp&(1 << 7)) >> 7;
  tmp = (unsigned) floor((x_x2 - -4.000000)/0.031250);
  conv[8] = (tmp&(1 << 0)) >> 0;
  conv[9] = (tmp&(1 << 1)) >> 1;
  conv[10] = (tmp&(1 << 2)) >> 2;
  conv[11] = (tmp&(1 << 3)) >> 3;
  conv[12] = (tmp&(1 << 4)) >> 4;
  conv[13] = (tmp&(1 << 5)) >> 5;
  conv[14] = (tmp&(1 << 6)) >> 6;
  conv[15] = (tmp&(1 << 7)) >> 7;
}

void AD_expl_x_x1(double x_x1, unsigned char *conv)
{
  unsigned tmp = (unsigned) floor((x_x1 - -3.454000)/0.026984);
  conv[0] = (tmp&(1 << 0)) >> 0;
  conv[1] = (tmp&(1 << 1)) >> 1;
  conv[2] = (tmp&(1 << 2)) >> 2;
  conv[3] = (tmp&(1 << 3)) >> 3;
  conv[4] = (tmp&(1 << 4)) >> 4;
  conv[5] = (tmp&(1 << 5)) >> 5;
  conv[6] = (tmp&(1 << 6)) >> 6;
  conv[7] = (tmp&(1 << 7)) >> 7;
}

void AD_expl_x_x2(double x_x2, unsigned char *conv)
{
  unsigned tmp = (unsigned) floor((x_x2 - -4.000000)/0.031250);
  conv[8] = (tmp&(1 << 0)) >> 0;
  conv[9] = (tmp&(1 << 1)) >> 1;
  conv[10] = (tmp&(1 << 2)) >> 2;
  conv[11] = (tmp&(1 << 3)) >> 3;
  conv[12] = (tmp&(1 << 4)) >> 4;
  conv[13] = (tmp&(1 << 5)) >> 5;
  conv[14] = (tmp&(1 << 6)) >> 6;
  conv[15] = (tmp&(1 << 7)) >> 7;
}

int K_with_AD_DA_expl(double x_x1, double x_x2, int *w_u)
{
  static unsigned char conv[DIM_X_VECTOR];
  static unsigned char answer[DIM_A_VECTOR];
  int res;
  AD_expl(x_x1, x_x2, conv);
  res = K(conv, answer);
  DA_expl(answer, w_u);
  return res;
}

int exists_K_with_AD_expl(double x_x1, double x_x2)
{
  static unsigned char conv[DIM_X_VECTOR];
  AD_expl(x_x1, x_x2, conv);
  return K_exists(conv);
}

int K(unsigned char *x, unsigned char *a)
{
  /* a[0] = w_u.0 */
  /* a[1] = w_u.1 */
  /* x[7] = x_x1.7 */
  /* x[15] = x_x2.7 */
  /* x[6] = x_x1.6 */
  /* x[14] = x_x2.6 */
  /* x[5] = x_x1.5 */
  /* x[13] = x_x2.5 */
  /* x[4] = x_x1.4 */
  /* x[12] = x_x2.4 */
  /* x[3] = x_x1.3 */
  /* x[11] = x_x2.3 */
  /* x[2] = x_x1.2 */
  /* x[10] = x_x2.2 */
  /* x[1] = x_x1.1 */
  /* x[9] = x_x2.1 */
  /* x[0] = x_x1.0 */
  /* x[8] = x_x2.0 */
  /* STATE VARS (X) */
  /* original var: x_x1 in [-3.454000, 3.454000] with step 0.026984 */
  /* original var: x_x2 in [-4.000000, 4.000000] with step 0.031250 */
  /* STATE VARS (Q) */
  /* ACTION VARS (U) */
  /* ACTION VARS (W) */
  /* original var: w_u in [-1, 1] */
  if (!K_exists(x)) return 1;
  a[0] = K_entries(x, 0);
  a[1] = K_entries(x, 1);
  return 0;
}

int K_exists(unsigned char *x)
{
  int return_bit = 0;
L_3a26321:
  if (x[7] == 1) goto L_3a261c0;
  else goto L_3a26300;
L_3a261c0:
  if (x[15] == 1) goto L_3a56700;
  else goto L_3a261a0;
L_3a56700:
  if (x[6] == 1) goto L_23f8620;
  else goto L_3a566e0;
L_23f8620:
  return return_bit;
L_3a566e0:
  if (x[14] == 1) goto L_23f8620;
  else goto L_3a566c0;
L_3a566c0:
  if (x[5] == 1) goto L_23f8620;
  else goto L_3a566a0;
L_3a566a0:
  if (x[13] == 1) goto L_23f8620;
  else goto L_3a56680;
L_3a56680:
  if (x[4] == 1) goto L_23f8620;
  else goto L_3a56660;
L_3a56660:
  if (x[12] == 1) goto L_23f8620;
  else goto L_3a56640;
L_3a56640:
  if (x[3] == 1) goto L_23f8620;
  else goto L_3a56620;
L_3a56620:
  if (x[11] == 1) goto L_23f8620;
  else goto L_3a56600;
L_3a56600:
  if (x[2] == 1) goto L_23f8620;
  else goto L_3a565e0;
L_3a565e0:
  if (x[10] == 1) goto L_23f8620;
  else goto L_3a99c40;
L_3a99c40:
  if (x[1] == 1) goto L_23f8a80;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_23f8a80:
  if (x[0] == 1) goto L_23f8620;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a261a0:
  if (x[6] == 1) goto L_23f8620;
  else goto L_3a26180;
L_3a26180:
  if (x[14] == 1) goto L_3a26160;
  else goto L_23f8620;
L_3a26160:
  if (x[5] == 1) goto L_23f8620;
  else goto L_3a26140;
L_3a26140:
  if (x[13] == 1) goto L_3a26120;
  else goto L_23f8620;
L_3a26120:
  if (x[4] == 1) goto L_23f8620;
  else goto L_3a26100;
L_3a26100:
  if (x[12] == 1) goto L_3a260e0;
  else goto L_23f8620;
L_3a260e0:
  if (x[3] == 1) goto L_23f8620;
  else goto L_3a260c0;
L_3a260c0:
  if (x[11] == 1) goto L_3a260a0;
  else goto L_23f8620;
L_3a260a0:
  if (x[2] == 1) goto L_23f8620;
  else {
    return_bit = !return_bit;
    goto L_3a25a20;
  }
L_3a25a20:
  if (x[10] == 1) goto L_23f8620;
  else goto L_3a31340;
L_3a31340:
  if (x[1] == 1) goto L_3a8f620;
  else goto L_3a1cdc0;
L_3a8f620:
  if (x[9] == 1) goto L_23f8620;
  else goto L_39dd500;
L_39dd500:
  if (x[0] == 1) goto L_23f8ac0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_23f8ac0:
  if (x[8] == 1) goto L_23f8620;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a1cdc0:
  if (x[9] == 1) goto L_39dd500;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a26300:
  if (x[15] == 1) goto L_3a262e0;
  else {
    return_bit = !return_bit;
    goto L_3a56580;
  }
L_3a262e0:
  if (x[6] == 1) goto L_3a262c0;
  else goto L_23f8620;
L_3a262c0:
  if (x[14] == 1) goto L_23f8620;
  else goto L_3a262a0;
L_3a262a0:
  if (x[5] == 1) goto L_3a26280;
  else goto L_23f8620;
L_3a26280:
  if (x[13] == 1) goto L_23f8620;
  else goto L_3a26260;
L_3a26260:
  if (x[4] == 1) goto L_3a26240;
  else goto L_23f8620;
L_3a26240:
  if (x[12] == 1) goto L_23f8620;
  else goto L_3a26220;
L_3a26220:
  if (x[3] == 1) goto L_3a26200;
  else goto L_23f8620;
L_3a26200:
  if (x[11] == 1) goto L_23f8620;
  else goto L_3a261e0;
L_3a261e0:
  if (x[2] == 1) goto L_3a25ba0;
  else goto L_23f8620;
L_3a25ba0:
  if (x[10] == 1) goto L_3a32520;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a32520:
  if (x[1] == 1) goto L_3a1e560;
  else goto L_3a6afe0;
L_3a1e560:
  if (x[9] == 1) goto L_23f8620;
  else goto L_26a26a0;
L_26a26a0:
  if (x[0] == 1) goto L_23f8620;
  else goto L_23f8ac0;
L_3a6afe0:
  if (x[9] == 1) goto L_26a26a0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a56580:
  if (x[6] == 1) goto L_3a56560;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a56560:
  if (x[14] == 1) goto L_3a56540;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a56540:
  if (x[5] == 1) goto L_3a56520;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a56520:
  if (x[13] == 1) goto L_3a56500;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a56500:
  if (x[4] == 1) goto L_3a564e0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a564e0:
  if (x[12] == 1) goto L_3a564c0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a564c0:
  if (x[3] == 1) goto L_3a564a0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a564a0:
  if (x[11] == 1) goto L_3a56480;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a56480:
  if (x[2] == 1) goto L_3a56460;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a56460:
  if (x[10] == 1) goto L_3a77500;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a77500:
  if (x[1] == 1) goto L_23f8620;
  else goto L_23f8a80;
}

int K_entries(unsigned char *x, unsigned bit)
{
  int return_bit;
  goto entry_point;
L_3a55fe1:
  if (x[7] == 1) goto L_3a55d20;
  else goto L_3a55fc0;
L_3a55d20:
  if (x[15] == 1) goto L_3a55bc0;
  else goto L_3a55d00;
L_3a55bc0:
  if (x[6] == 1) goto L_23f8620;
  else goto L_3a55ba0;
L_23f8620:
  return return_bit;
L_3a55ba0:
  if (x[14] == 1) goto L_23f8620;
  else goto L_3a55b80;
L_3a55b80:
  if (x[5] == 1) goto L_23f8620;
  else goto L_3a55b60;
L_3a55b60:
  if (x[13] == 1) goto L_23f8620;
  else goto L_3a55b40;
L_3a55b40:
  if (x[4] == 1) goto L_23f8620;
  else goto L_3a55b20;
L_3a55b20:
  if (x[12] == 1) goto L_23f8620;
  else goto L_3a55b00;
L_3a55b00:
  if (x[3] == 1) goto L_23f8620;
  else goto L_3a55ae0;
L_3a55ae0:
  if (x[11] == 1) goto L_23f8620;
  else goto L_3a55ac0;
L_3a55ac0:
  if (x[2] == 1) goto L_23f8620;
  else goto L_3a55aa0;
L_3a55aa0:
  if (x[10] == 1) goto L_23f8620;
  else goto L_3a60ba0;
L_3a60ba0:
  if (x[1] == 1) goto L_3a77be0;
  else goto L_3a0d860;
L_3a77be0:
  if (x[9] == 1) goto L_26a26a0;
  else goto L_23f8a80;
L_26a26a0:
  if (x[0] == 1) goto L_23f8620;
  else goto L_23f8ac0;
L_23f8ac0:
  if (x[8] == 1) goto L_23f8620;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_23f8a80:
  if (x[0] == 1) goto L_23f8620;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a0d860:
  if (x[9] == 1) goto L_23f8ac0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55d00:
  if (x[6] == 1) goto L_23f8620;
  else goto L_3a55ce0;
L_3a55ce0:
  if (x[14] == 1) goto L_3a55cc0;
  else goto L_23f8620;
L_3a55cc0:
  if (x[5] == 1) goto L_23f8620;
  else goto L_3a55ca0;
L_3a55ca0:
  if (x[13] == 1) goto L_3a55c80;
  else goto L_23f8620;
L_3a55c80:
  if (x[4] == 1) goto L_23f8620;
  else goto L_3a55c60;
L_3a55c60:
  if (x[12] == 1) goto L_3a55c40;
  else goto L_23f8620;
L_3a55c40:
  if (x[3] == 1) goto L_23f8620;
  else goto L_3a55c20;
L_3a55c20:
  if (x[11] == 1) goto L_3a55c00;
  else goto L_23f8620;
L_3a55c00:
  if (x[2] == 1) goto L_23f8620;
  else goto L_3a55be0;
L_3a55be0:
  if (x[10] == 1) goto L_3a61100;
  else goto L_23f8620;
L_3a61100:
  if (x[1] == 1) goto L_3a1cdc0;
  else {
    return_bit = !return_bit;
    goto L_39cdde0;
  }
L_3a1cdc0:
  if (x[9] == 1) goto L_39dd500;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_39dd500:
  if (x[0] == 1) goto L_23f8ac0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_39cdde0:
  if (x[9] == 1) goto L_23f8620;
  else goto L_23f8ac0;
L_3a55fc0:
  if (x[15] == 1) goto L_3a55e60;
  else {
    return_bit = !return_bit;
    goto L_3a55fa0;
  }
L_3a55e60:
  if (x[6] == 1) goto L_3a55e40;
  else goto L_23f8620;
L_3a55e40:
  if (x[14] == 1) goto L_23f8620;
  else goto L_3a55e20;
L_3a55e20:
  if (x[5] == 1) goto L_3a55e00;
  else goto L_23f8620;
L_3a55e00:
  if (x[13] == 1) goto L_23f8620;
  else goto L_3a55de0;
L_3a55de0:
  if (x[4] == 1) goto L_3a55dc0;
  else goto L_23f8620;
L_3a55dc0:
  if (x[12] == 1) goto L_23f8620;
  else goto L_3a55da0;
L_3a55da0:
  if (x[3] == 1) goto L_3a55d80;
  else goto L_23f8620;
L_3a55d80:
  if (x[11] == 1) goto L_23f8620;
  else goto L_3a55d60;
L_3a55d60:
  if (x[2] == 1) goto L_3a55d40;
  else goto L_23f8620;
L_3a55d40:
  if (x[10] == 1) goto L_23f8620;
  else goto L_3a618c0;
L_3a618c0:
  if (x[1] == 1) goto L_3a0d860;
  else {
    return_bit = !return_bit;
    goto L_3a1e560;
  }
L_3a1e560:
  if (x[9] == 1) goto L_23f8620;
  else goto L_26a26a0;
L_3a55fa0:
  if (x[6] == 1) goto L_3a55f80;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55f80:
  if (x[14] == 1) goto L_3a55f60;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55f60:
  if (x[5] == 1) goto L_3a55f40;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55f40:
  if (x[13] == 1) goto L_3a55f20;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55f20:
  if (x[4] == 1) goto L_3a55f00;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55f00:
  if (x[12] == 1) goto L_3a55ee0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55ee0:
  if (x[3] == 1) goto L_3a55ec0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55ec0:
  if (x[11] == 1) goto L_3a55ea0;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55ea0:
  if (x[2] == 1) goto L_3a55e80;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a55e80:
  if (x[10] == 1) goto L_3a61a60;
  else {
    return_bit = !return_bit;
    goto L_23f8620;
  }
L_3a61a60:
  if (x[1] == 1) goto L_39cdde0;
  else goto L_3a1d580;
L_3a1d580:
  if (x[9] == 1) goto L_23f8a80;
  else goto L_39dd500;
L_3a528e1:
  if (x[7] == 1) goto L_3a52620;
  else goto L_3a528c0;
L_3a52620:
  if (x[15] == 1) goto L_23f8620;
  else goto L_3a52600;
L_3a52600:
  if (x[6] == 1) goto L_23f8620;
  else goto L_3a525e0;
L_3a525e0:
  if (x[14] == 1) goto L_3a525c0;
  else goto L_23f8620;
L_3a525c0:
  if (x[5] == 1) goto L_23f8620;
  else goto L_3a525a0;
L_3a525a0:
  if (x[13] == 1) goto L_3a52580;
  else goto L_23f8620;
L_3a52580:
  if (x[4] == 1) goto L_23f8620;
  else goto L_3a52560;
L_3a52560:
  if (x[12] == 1) goto L_3a52540;
  else goto L_23f8620;
L_3a52540:
  if (x[3] == 1) goto L_23f8620;
  else goto L_3a52520;
L_3a52520:
  if (x[11] == 1) goto L_3a52500;
  else goto L_23f8620;
L_3a52500:
  if (x[2] == 1) goto L_23f8620;
  else goto L_3a524e0;
L_3a524e0:
  if (x[10] == 1) goto L_3a65320;
  else {
    return_bit = !return_bit;
    goto L_3a31340;
  }
L_3a65320:
  if (x[1] == 1) goto L_23f8620;
  else goto L_39cdde0;
L_3a31340:
  if (x[1] == 1) goto L_3a8f620;
  else goto L_3a1cdc0;
L_3a8f620:
  if (x[9] == 1) goto L_23f8620;
  else goto L_39dd500;
L_3a528c0:
  if (x[15] == 1) goto L_3a52740;
  else goto L_3a528a0;
L_3a52740:
  if (x[6] == 1) goto L_3a52720;
  else goto L_23f8620;
L_3a52720:
  if (x[14] == 1) goto L_23f8620;
  else goto L_3a52700;
L_3a52700:
  if (x[5] == 1) goto L_3a526e0;
  else goto L_23f8620;
L_3a526e0:
  if (x[13] == 1) goto L_23f8620;
  else goto L_3a526c0;
L_3a526c0:
  if (x[4] == 1) goto L_3a526a0;
  else goto L_23f8620;
L_3a526a0:
  if (x[12] == 1) goto L_23f8620;
  else goto L_3a52680;
L_3a52680:
  if (x[3] == 1) goto L_3a52660;
  else goto L_23f8620;
L_3a52660:
  if (x[11] == 1) goto L_23f8620;
  else goto L_3a52640;
L_3a52640:
  if (x[2] == 1) goto L_3a7b1e0;
  else goto L_23f8620;
L_3a7b1e0:
  if (x[10] == 1) goto L_23f8620;
  else goto L_3a6d9c0;
L_3a6d9c0:
  if (x[1] == 1) goto L_23f8620;
  else goto L_3a1e560;
L_3a528a0:
  if (x[6] == 1) goto L_3a52880;
  else goto L_23f8620;
L_3a52880:
  if (x[14] == 1) goto L_3a52860;
  else goto L_23f8620;
L_3a52860:
  if (x[5] == 1) goto L_3a52840;
  else goto L_23f8620;
L_3a52840:
  if (x[13] == 1) goto L_3a52820;
  else goto L_23f8620;
L_3a52820:
  if (x[4] == 1) goto L_3a52800;
  else goto L_23f8620;
L_3a52800:
  if (x[12] == 1) goto L_3a527e0;
  else goto L_23f8620;
L_3a527e0:
  if (x[3] == 1) goto L_3a527c0;
  else goto L_23f8620;
L_3a527c0:
  if (x[11] == 1) goto L_3a527a0;
  else goto L_23f8620;
L_3a527a0:
  if (x[2] == 1) goto L_3a52780;
  else goto L_23f8620;
L_3a52780:
  if (x[10] == 1) goto L_3a52760;
  else goto L_23f8620;
L_3a52760:
  if (x[1] == 1) goto L_39cdde0;
  else goto L_3abbca0;
L_3abbca0:
  if (x[9] == 1) goto L_23f8620;
  else goto L_3abbba0;
L_3abbba0:
  if (x[0] == 1) goto L_23f8ac0;
  else goto L_23f8620;
entry_point:
  switch(bit) {
    case 0:
      return_bit = 0;
      goto L_3a55fe1;
    case 1:
      return_bit = 0;
      goto L_3a528e1;
  }
}

