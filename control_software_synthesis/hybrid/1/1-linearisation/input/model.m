Const
  T    : 0.01;
  g    : 9.8;
  l    : g;
  m    : 1 / (l*l);
  mu    : 0.01;
  mult_u  : 0.5;
  goal_x1_tol : 0.1;
  goal_x2_tol : 0.1;

  pi    : 3.14;
  lb_x1   : -1.1 * pi;
  ub_x1   : 1.1 * pi;
  lb_x2   : -4;
  ub_x2   : 4;
  lb_u  : -1;
  ub_u  : 1;
  lb_q  : -4;
  ub_q  : 4;

  A : pi / 2;

  lb_y1   : 0;
  ub_y1   : 255;
  lb_y2   : 0;
  ub_y2   : 255;
  deltax1 : (ub_x1 - lb_x1) / (ub_y1 - lb_y1 + 1);
  deltax2 : (ub_x2 - lb_x2) / (ub_y2 - lb_y2 + 1);

Type
  x1_interval : [lb_x1, ub_x1];
  x2_interval : [lb_x2, ub_x2];
  u_interval  : lb_u .. ub_u;

Statevars
  x1 : x1_interval;
  x2 : x2_interval;

Inputvars
  u : u_interval;

Outputvars
  y1 : lb_y1 .. ub_y1;
  y2 : lb_y2 .. ub_y2;

Functions
  sin : 1 : (2 * pi);

Trans
  Exists
    q : -1 .. 1,
    above: 0 .. 1
  {
    x1' = x1 + 2*pi*q + T*x2;
    (above = 1) -> (x1 >= -A) & (x1 <= A);
    (above = 1) -> x2' = x2 + T * ((g / l) * sin(x1, 0.5) + (1 / (m*l*l)) *  mult_u * u);
    (above = 0) -> (x1 <= -A) | (x1 >= A);
    (above = 0) -> x2' = x2 + T * ((g / l) * sin(x1, 0.5) - (mu / (m*l*l)) * x2 + (1 / (m*l*l)) *  mult_u * u);
    -pi <= x1;
    x1 <= pi;
    -pi <= x1';
    x1' <= pi;
  };

ControllableRegion
  (lb_x1 <= x1) & (x1 <= ub_x1);
  (lb_x2 <= x2) & (x2 <= ub_x2);

Goal
  -goal_x1_tol <= x1  &  x1 <= goal_x1_tol;
  -goal_x2_tol <= x2  &  x2 <= goal_x2_tol;

Observation
  Exists
    dx1 : [0, deltax1],
    dx2 : [0, deltax2]
  {
    x1 = lb_x1 + deltax1 * y1 + dx1;
    x2 = lb_x2 + deltax2 * y2 + dx2;
  };

