set terminal jpeg
set output "ctrl_reg.exists_strong_ctrl.act.jpg"
unset key
set xrange [-3.454000:3.454000]
set xlabel "w_u"
set yrange [0:1]
set ylabel ""
#set grid
set xtics ("(-1),(0),(1)" 0.000000)
set object rectangle from -3.454000 , 0 to 3.454000 , 1 fs solid 0.3 noborder fc rgb "#79F3A1"
plot -4.000000, 4.000000
