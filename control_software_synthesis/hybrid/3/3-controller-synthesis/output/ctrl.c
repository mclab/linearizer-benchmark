#include "ctrl.h"
#include <math.h>

int K_entries(unsigned char *, unsigned);

void DA(unsigned char *to_be_conv, double *conv)
{
  int tmp;
  tmp = 0;
  tmp += to_be_conv[0] << 0;
  tmp += to_be_conv[1] << 1;
  conv[0] = -1 + tmp;
}

void AD(double *to_be_conv, unsigned char *conv)
{
  unsigned tmp;
  tmp = (unsigned) floor((to_be_conv[0] - -3.454000)/0.013492);
  conv[0] = (tmp&(1 << 0)) >> 0;
  conv[1] = (tmp&(1 << 1)) >> 1;
  conv[2] = (tmp&(1 << 2)) >> 2;
  conv[3] = (tmp&(1 << 3)) >> 3;
  conv[4] = (tmp&(1 << 4)) >> 4;
  conv[5] = (tmp&(1 << 5)) >> 5;
  conv[6] = (tmp&(1 << 6)) >> 6;
  conv[7] = (tmp&(1 << 7)) >> 7;
  conv[8] = (tmp&(1 << 8)) >> 8;
  tmp = (unsigned) floor((to_be_conv[1] - -4.000000)/0.015625);
  conv[9] = (tmp&(1 << 0)) >> 0;
  conv[10] = (tmp&(1 << 1)) >> 1;
  conv[11] = (tmp&(1 << 2)) >> 2;
  conv[12] = (tmp&(1 << 3)) >> 3;
  conv[13] = (tmp&(1 << 4)) >> 4;
  conv[14] = (tmp&(1 << 5)) >> 5;
  conv[15] = (tmp&(1 << 6)) >> 6;
  conv[16] = (tmp&(1 << 7)) >> 7;
  conv[17] = (tmp&(1 << 8)) >> 8;
}

int K_with_AD_DA(double *input, double *output)
{
  static unsigned char conv[DIM_X_VECTOR];
  static unsigned char answer[DIM_A_VECTOR];
  int res;
  AD(input, conv);
  res = K(conv, answer);
  DA(answer, output);
  return res;
}

int exists_K_with_AD(double *input)
{
  static unsigned char conv[DIM_X_VECTOR];
  AD(input, conv);
  return K_exists(conv);
}

void DA_expl(unsigned char *to_be_conv, int *w_u)
{
  int tmp;
  tmp = 0;
  tmp += to_be_conv[0] << 0;
  tmp += to_be_conv[1] << 1;
  *w_u = -1 + tmp;
}

void DA_expl_w_u(unsigned char *to_be_conv, int *w_u)
{
  unsigned tmp = 0;
  tmp += to_be_conv[0] << 0;
  tmp += to_be_conv[1] << 1;
  *w_u = -1 + tmp;
}

void AD_expl(double x_x1, double x_x2, unsigned char *conv)
{
  unsigned tmp;
  tmp = (unsigned) floor((x_x1 - -3.454000)/0.013492);
  conv[0] = (tmp&(1 << 0)) >> 0;
  conv[1] = (tmp&(1 << 1)) >> 1;
  conv[2] = (tmp&(1 << 2)) >> 2;
  conv[3] = (tmp&(1 << 3)) >> 3;
  conv[4] = (tmp&(1 << 4)) >> 4;
  conv[5] = (tmp&(1 << 5)) >> 5;
  conv[6] = (tmp&(1 << 6)) >> 6;
  conv[7] = (tmp&(1 << 7)) >> 7;
  conv[8] = (tmp&(1 << 8)) >> 8;
  tmp = (unsigned) floor((x_x2 - -4.000000)/0.015625);
  conv[9] = (tmp&(1 << 0)) >> 0;
  conv[10] = (tmp&(1 << 1)) >> 1;
  conv[11] = (tmp&(1 << 2)) >> 2;
  conv[12] = (tmp&(1 << 3)) >> 3;
  conv[13] = (tmp&(1 << 4)) >> 4;
  conv[14] = (tmp&(1 << 5)) >> 5;
  conv[15] = (tmp&(1 << 6)) >> 6;
  conv[16] = (tmp&(1 << 7)) >> 7;
  conv[17] = (tmp&(1 << 8)) >> 8;
}

void AD_expl_x_x1(double x_x1, unsigned char *conv)
{
  unsigned tmp = (unsigned) floor((x_x1 - -3.454000)/0.013492);
  conv[0] = (tmp&(1 << 0)) >> 0;
  conv[1] = (tmp&(1 << 1)) >> 1;
  conv[2] = (tmp&(1 << 2)) >> 2;
  conv[3] = (tmp&(1 << 3)) >> 3;
  conv[4] = (tmp&(1 << 4)) >> 4;
  conv[5] = (tmp&(1 << 5)) >> 5;
  conv[6] = (tmp&(1 << 6)) >> 6;
  conv[7] = (tmp&(1 << 7)) >> 7;
  conv[8] = (tmp&(1 << 8)) >> 8;
}

void AD_expl_x_x2(double x_x2, unsigned char *conv)
{
  unsigned tmp = (unsigned) floor((x_x2 - -4.000000)/0.015625);
  conv[9] = (tmp&(1 << 0)) >> 0;
  conv[10] = (tmp&(1 << 1)) >> 1;
  conv[11] = (tmp&(1 << 2)) >> 2;
  conv[12] = (tmp&(1 << 3)) >> 3;
  conv[13] = (tmp&(1 << 4)) >> 4;
  conv[14] = (tmp&(1 << 5)) >> 5;
  conv[15] = (tmp&(1 << 6)) >> 6;
  conv[16] = (tmp&(1 << 7)) >> 7;
  conv[17] = (tmp&(1 << 8)) >> 8;
}

int K_with_AD_DA_expl(double x_x1, double x_x2, int *w_u)
{
  static unsigned char conv[DIM_X_VECTOR];
  static unsigned char answer[DIM_A_VECTOR];
  int res;
  AD_expl(x_x1, x_x2, conv);
  res = K(conv, answer);
  DA_expl(answer, w_u);
  return res;
}

int exists_K_with_AD_expl(double x_x1, double x_x2)
{
  static unsigned char conv[DIM_X_VECTOR];
  AD_expl(x_x1, x_x2, conv);
  return K_exists(conv);
}

int K(unsigned char *x, unsigned char *a)
{
  /* a[0] = w_u.0 */
  /* a[1] = w_u.1 */
  /* x[8] = x_x1.8 */
  /* x[17] = x_x2.8 */
  /* x[7] = x_x1.7 */
  /* x[16] = x_x2.7 */
  /* x[6] = x_x1.6 */
  /* x[15] = x_x2.6 */
  /* x[5] = x_x1.5 */
  /* x[14] = x_x2.5 */
  /* x[4] = x_x1.4 */
  /* x[13] = x_x2.4 */
  /* x[3] = x_x1.3 */
  /* x[12] = x_x2.3 */
  /* x[2] = x_x1.2 */
  /* x[11] = x_x2.2 */
  /* x[1] = x_x1.1 */
  /* x[10] = x_x2.1 */
  /* x[0] = x_x1.0 */
  /* x[9] = x_x2.0 */
  /* STATE VARS (X) */
  /* original var: x_x1 in [-3.454000, 3.454000] with step 0.013492 */
  /* original var: x_x2 in [-4.000000, 4.000000] with step 0.015625 */
  /* STATE VARS (Q) */
  /* ACTION VARS (U) */
  /* ACTION VARS (W) */
  /* original var: w_u in [-1, 1] */
  if (!K_exists(x)) return 1;
  a[0] = K_entries(x, 0);
  a[1] = K_entries(x, 1);
  return 0;
}

int K_exists(unsigned char *x)
{
  int return_bit = 0;
L_2b0c761:
  if (x[8] == 1) goto L_2b0c600;
  else goto L_2b0c740;
L_2b0c600:
  if (x[17] == 1) goto L_2a77ee0;
  else goto L_2b0c5e0;
L_2a77ee0:
  if (x[7] == 1) goto L_149c620;
  else goto L_2a77ec0;
L_149c620:
  return return_bit;
L_2a77ec0:
  if (x[16] == 1) goto L_149c620;
  else goto L_2a77ea0;
L_2a77ea0:
  if (x[6] == 1) goto L_149c620;
  else goto L_2a77e80;
L_2a77e80:
  if (x[15] == 1) goto L_149c620;
  else goto L_2a77e60;
L_2a77e60:
  if (x[5] == 1) goto L_149c620;
  else goto L_2a77e40;
L_2a77e40:
  if (x[14] == 1) goto L_149c620;
  else goto L_2a77e20;
L_2a77e20:
  if (x[4] == 1) goto L_149c620;
  else goto L_2a77e00;
L_2a77e00:
  if (x[13] == 1) goto L_149c620;
  else goto L_2a77de0;
L_2a77de0:
  if (x[3] == 1) goto L_149c620;
  else goto L_2a77dc0;
L_2a77dc0:
  if (x[12] == 1) goto L_149c620;
  else goto L_2a81a60;
L_2a81a60:
  if (x[2] == 1) goto L_2a81520;
  else goto L_2a80a20;
L_2a81520:
  if (x[11] == 1) goto L_149c620;
  else goto L_2a81500;
L_2a81500:
  if (x[1] == 1) goto L_2a29620;
  else goto L_149d620;
L_2a29620:
  if (x[10] == 1) goto L_2ad2a20;
  else goto L_149cb00;
L_2ad2a20:
  if (x[0] == 1) goto L_149c620;
  else goto L_149cb40;
L_149cb40:
  if (x[9] == 1) goto L_149c620;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_149cb00:
  if (x[0] == 1) goto L_149c620;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_149d620:
  if (x[10] == 1) goto L_149cb40;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a80a20:
  if (x[11] == 1) goto L_149c620;
  else goto L_149d620;
L_2b0c5e0:
  if (x[7] == 1) goto L_149c620;
  else goto L_2b0c5c0;
L_2b0c5c0:
  if (x[16] == 1) goto L_2b0c5a0;
  else goto L_149c620;
L_2b0c5a0:
  if (x[6] == 1) goto L_149c620;
  else goto L_2b0c580;
L_2b0c580:
  if (x[15] == 1) goto L_2b0c560;
  else goto L_149c620;
L_2b0c560:
  if (x[5] == 1) goto L_149c620;
  else goto L_2b0c540;
L_2b0c540:
  if (x[14] == 1) goto L_2b0c520;
  else goto L_149c620;
L_2b0c520:
  if (x[4] == 1) goto L_149c620;
  else goto L_2b0c120;
L_2b0c120:
  if (x[13] == 1) goto L_2b0c0e0;
  else goto L_149c620;
L_2b0c0e0:
  if (x[3] == 1) goto L_2b40ec0;
  else goto L_2a2d360;
L_2b40ec0:
  if (x[12] == 1) goto L_149c620;
  else goto L_2b40ea0;
L_2b40ea0:
  if (x[2] == 1) goto L_149c620;
  else goto L_2a563c0;
L_2a563c0:
  if (x[11] == 1) goto L_149c620;
  else goto L_2a563a0;
L_2a563a0:
  if (x[1] == 1) goto L_149c620;
  else goto L_2a56380;
L_2a56380:
  if (x[10] == 1) goto L_149c620;
  else goto L_149cb00;
L_2a2d360:
  if (x[12] == 1) goto L_2a77f20;
  else {
    return_bit = !return_bit;
    goto L_2a352e0;
  }
L_2a77f20:
  if (x[2] == 1) goto L_2a18c40;
  else {
    return_bit = !return_bit;
    goto L_2a77f00;
  }
L_2a18c40:
  if (x[11] == 1) goto L_2a4eac0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a4eac0:
  if (x[1] == 1) goto L_2a4eaa0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a4eaa0:
  if (x[10] == 1) goto L_149cb00;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a77f00:
  if (x[11] == 1) goto L_2abcae0;
  else goto L_2b073c0;
L_2abcae0:
  if (x[1] == 1) goto L_149c620;
  else goto L_2ad2a20;
L_2b073c0:
  if (x[1] == 1) goto L_149c620;
  else goto L_149cb00;
L_2a352e0:
  if (x[2] == 1) goto L_2a41ba0;
  else goto L_2afc1a0;
L_2a41ba0:
  if (x[11] == 1) goto L_149c620;
  else goto L_2afc180;
L_2afc180:
  if (x[1] == 1) goto L_2b0d240;
  else goto L_2ab7f20;
L_2b0d240:
  if (x[10] == 1) goto L_149c620;
  else goto L_2ab7e00;
L_2ab7e00:
  if (x[0] == 1) goto L_149cb40;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2ab7f20:
  if (x[10] == 1) goto L_2ab7e00;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2afc1a0:
  if (x[11] == 1) goto L_2afc180;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2b0c740:
  if (x[17] == 1) goto L_2b0c720;
  else {
    return_bit = !return_bit;
    goto L_2a78380;
  }
L_2b0c720:
  if (x[7] == 1) goto L_2b0c700;
  else goto L_149c620;
L_2b0c700:
  if (x[16] == 1) goto L_149c620;
  else goto L_2b0c6e0;
L_2b0c6e0:
  if (x[6] == 1) goto L_2b0c6c0;
  else goto L_149c620;
L_2b0c6c0:
  if (x[15] == 1) goto L_149c620;
  else goto L_2b0c6a0;
L_2b0c6a0:
  if (x[5] == 1) goto L_2b0c680;
  else goto L_149c620;
L_2b0c680:
  if (x[14] == 1) goto L_149c620;
  else goto L_2b0c660;
L_2b0c660:
  if (x[4] == 1) goto L_2b0c640;
  else goto L_149c620;
L_2b0c640:
  if (x[13] == 1) goto L_149c620;
  else goto L_2b0c620;
L_2b0c620:
  if (x[3] == 1) goto L_2a2d500;
  else {
    return_bit = !return_bit;
    goto L_2b060c0;
  }
L_2a2d500:
  if (x[12] == 1) goto L_2a394a0;
  else goto L_2a78100;
L_2a394a0:
  if (x[2] == 1) goto L_2affba0;
  else goto L_2a403e0;
L_2affba0:
  if (x[11] == 1) goto L_149c620;
  else goto L_2affb80;
L_2affb80:
  if (x[1] == 1) goto L_2ad2b40;
  else goto L_2b0c280;
L_2ad2b40:
  if (x[10] == 1) goto L_149c620;
  else goto L_2ad2a20;
L_2b0c280:
  if (x[10] == 1) goto L_2ad2a20;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a403e0:
  if (x[11] == 1) goto L_2affb80;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a78100:
  if (x[2] == 1) goto L_2a780e0;
  else {
    return_bit = !return_bit;
    goto L_2a563c0;
  }
L_2a780e0:
  if (x[11] == 1) goto L_2a9c7c0;
  else goto L_2b09d40;
L_2a9c7c0:
  if (x[1] == 1) goto L_149cb00;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2b09d40:
  if (x[1] == 1) goto L_2ab7e00;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2b060c0:
  if (x[12] == 1) goto L_2b060a0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2b060a0:
  if (x[2] == 1) goto L_2a18c40;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a78380:
  if (x[7] == 1) goto L_2a78360;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a78360:
  if (x[16] == 1) goto L_2a78340;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a78340:
  if (x[6] == 1) goto L_2a78320;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a78320:
  if (x[15] == 1) goto L_2a78300;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a78300:
  if (x[5] == 1) goto L_2a782e0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a782e0:
  if (x[14] == 1) goto L_2a782c0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a782c0:
  if (x[4] == 1) goto L_2a782a0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a782a0:
  if (x[13] == 1) goto L_2a78280;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a78280:
  if (x[3] == 1) goto L_2a78260;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a78260:
  if (x[12] == 1) goto L_2a81300;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a81300:
  if (x[2] == 1) goto L_2a810e0;
  else goto L_2a812e0;
L_2a810e0:
  if (x[11] == 1) goto L_149d700;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_149d700:
  if (x[10] == 1) goto L_149c620;
  else goto L_149cb40;
L_2a812e0:
  if (x[11] == 1) goto L_2a812c0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a812c0:
  if (x[1] == 1) goto L_149d700;
  else goto L_2ab6a20;
L_2ab6a20:
  if (x[10] == 1) goto L_149cb00;
  else goto L_2ab7e00;
}

int K_entries(unsigned char *x, unsigned bit)
{
  int return_bit;
  goto entry_point;
L_2a77001:
  if (x[8] == 1) goto L_2a76ce0;
  else goto L_2a76fe0;
L_2a76ce0:
  if (x[17] == 1) goto L_2a76ae0;
  else goto L_2a76ca0;
L_2a76ae0:
  if (x[7] == 1) goto L_149c620;
  else goto L_2a76ac0;
L_149c620:
  return return_bit;
L_2a76ac0:
  if (x[16] == 1) goto L_149c620;
  else goto L_2a76aa0;
L_2a76aa0:
  if (x[6] == 1) goto L_149c620;
  else goto L_2a76a60;
L_2a76a60:
  if (x[15] == 1) goto L_149c620;
  else goto L_2a76a40;
L_2a76a40:
  if (x[5] == 1) goto L_149c620;
  else goto L_2a76a20;
L_2a76a20:
  if (x[14] == 1) goto L_149c620;
  else goto L_2a76a00;
L_2a76a00:
  if (x[4] == 1) goto L_149c620;
  else goto L_2a769e0;
L_2a769e0:
  if (x[13] == 1) goto L_149c620;
  else goto L_2a769c0;
L_2a769c0:
  if (x[3] == 1) goto L_149c620;
  else goto L_2a72ae0;
L_2a72ae0:
  if (x[12] == 1) goto L_149c620;
  else goto L_2a7d520;
L_2a7d520:
  if (x[2] == 1) goto L_2a7d500;
  else goto L_149d720;
L_2a7d500:
  if (x[11] == 1) goto L_149c620;
  else goto L_2a7d4e0;
L_2a7d4e0:
  if (x[1] == 1) goto L_2ad2b40;
  else goto L_149d700;
L_2ad2b40:
  if (x[10] == 1) goto L_149c620;
  else goto L_2ad2a20;
L_2ad2a20:
  if (x[0] == 1) goto L_149c620;
  else goto L_149cb40;
L_149cb40:
  if (x[9] == 1) goto L_149c620;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_149d700:
  if (x[10] == 1) goto L_149c620;
  else goto L_149cb40;
L_149d720:
  if (x[11] == 1) goto L_149c620;
  else goto L_149d700;
L_2a76ca0:
  if (x[7] == 1) goto L_149c620;
  else goto L_2a76c00;
L_2a76c00:
  if (x[16] == 1) goto L_2a76be0;
  else goto L_149c620;
L_2a76be0:
  if (x[6] == 1) goto L_149c620;
  else goto L_2a76bc0;
L_2a76bc0:
  if (x[15] == 1) goto L_2a76ba0;
  else goto L_149c620;
L_2a76ba0:
  if (x[5] == 1) goto L_149c620;
  else goto L_2a76b80;
L_2a76b80:
  if (x[14] == 1) goto L_2a76b60;
  else goto L_149c620;
L_2a76b60:
  if (x[4] == 1) goto L_149c620;
  else goto L_2a76b40;
L_2a76b40:
  if (x[13] == 1) goto L_2a76b20;
  else goto L_149c620;
L_2a76b20:
  if (x[3] == 1) goto L_149c620;
  else goto L_2a76b00;
L_2a76b00:
  if (x[12] == 1) goto L_2a7e2a0;
  else goto L_149c620;
L_2a7e2a0:
  if (x[2] == 1) goto L_2a7de00;
  else {
    return_bit = !return_bit;
    goto L_2a7e280;
  }
L_2a7de00:
  if (x[11] == 1) goto L_2a7d460;
  else {
    return_bit = !return_bit;
    goto L_149cac0;
  }
L_2a7d460:
  if (x[1] == 1) goto L_2ab6a20;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2ab6a20:
  if (x[10] == 1) goto L_149cb00;
  else goto L_2ab7e00;
L_149cb00:
  if (x[0] == 1) goto L_149c620;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2ab7e00:
  if (x[0] == 1) goto L_149cb40;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_149cac0:
  if (x[10] == 1) goto L_149c620;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a7e280:
  if (x[11] == 1) goto L_2aecb20;
  else goto L_2a7dc20;
L_2aecb20:
  if (x[1] == 1) goto L_149c620;
  else goto L_2a29620;
L_2a29620:
  if (x[10] == 1) goto L_2ad2a20;
  else goto L_149cb00;
L_2a7dc20:
  if (x[1] == 1) goto L_149cac0;
  else goto L_2a4eaa0;
L_2a4eaa0:
  if (x[10] == 1) goto L_149cb00;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76fe0:
  if (x[17] == 1) goto L_2a76ea0;
  else {
    return_bit = !return_bit;
    goto L_2a76fc0;
  }
L_2a76ea0:
  if (x[7] == 1) goto L_2a76e80;
  else goto L_149c620;
L_2a76e80:
  if (x[16] == 1) goto L_149c620;
  else goto L_2a76e40;
L_2a76e40:
  if (x[6] == 1) goto L_2a76e20;
  else goto L_149c620;
L_2a76e20:
  if (x[15] == 1) goto L_149c620;
  else goto L_2a76da0;
L_2a76da0:
  if (x[5] == 1) goto L_2a76d80;
  else goto L_149c620;
L_2a76d80:
  if (x[14] == 1) goto L_149c620;
  else goto L_2a76d60;
L_2a76d60:
  if (x[4] == 1) goto L_2a76d40;
  else goto L_149c620;
L_2a76d40:
  if (x[13] == 1) goto L_149c620;
  else goto L_2a76d20;
L_2a76d20:
  if (x[3] == 1) goto L_2a76d00;
  else goto L_149c620;
L_2a76d00:
  if (x[12] == 1) goto L_149c620;
  else goto L_2a7f4a0;
L_2a7f4a0:
  if (x[2] == 1) goto L_2a7f480;
  else goto L_2a7ece0;
L_2a7f480:
  if (x[11] == 1) goto L_2a7e760;
  else goto L_2a7d460;
L_2a7e760:
  if (x[1] == 1) goto L_2a56380;
  else goto L_149cac0;
L_2a56380:
  if (x[10] == 1) goto L_149c620;
  else goto L_149cb00;
L_2a7ece0:
  if (x[11] == 1) goto L_149cac0;
  else {
    return_bit = !return_bit;
    goto L_2aecb20;
  }
L_2a76fc0:
  if (x[7] == 1) goto L_2a76fa0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76fa0:
  if (x[16] == 1) goto L_2a76f80;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76f80:
  if (x[6] == 1) goto L_2a76f60;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76f60:
  if (x[15] == 1) goto L_2a76f40;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76f40:
  if (x[5] == 1) goto L_2a76f20;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76f20:
  if (x[14] == 1) goto L_2a76f00;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76f00:
  if (x[4] == 1) goto L_2a76ee0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76ee0:
  if (x[13] == 1) goto L_2a76ec0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a76ec0:
  if (x[3] == 1) goto L_2a7fe60;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a7fe60:
  if (x[12] == 1) goto L_2a7f180;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a7f180:
  if (x[2] == 1) goto L_149d640;
  else goto L_2a7e9c0;
L_149d640:
  if (x[11] == 1) goto L_149d620;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_149d620:
  if (x[10] == 1) goto L_149cb40;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a7e9c0:
  if (x[11] == 1) goto L_2a7e9a0;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2a7e9a0:
  if (x[1] == 1) goto L_149d620;
  else goto L_2ab7f20;
L_2ab7f20:
  if (x[10] == 1) goto L_2ab7e00;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2aa4961:
  if (x[8] == 1) goto L_2aa4520;
  else goto L_2aa4940;
L_2aa4520:
  if (x[17] == 1) goto L_149c620;
  else goto L_2aa4500;
L_2aa4500:
  if (x[7] == 1) goto L_149c620;
  else goto L_2aa44e0;
L_2aa44e0:
  if (x[16] == 1) goto L_2aa44a0;
  else goto L_149c620;
L_2aa44a0:
  if (x[6] == 1) goto L_149c620;
  else goto L_2aa4480;
L_2aa4480:
  if (x[15] == 1) goto L_2aa4460;
  else goto L_149c620;
L_2aa4460:
  if (x[5] == 1) goto L_149c620;
  else goto L_2aa4440;
L_2aa4440:
  if (x[14] == 1) goto L_2aa4420;
  else goto L_149c620;
L_2aa4420:
  if (x[4] == 1) goto L_149c620;
  else goto L_2aa4400;
L_2aa4400:
  if (x[13] == 1) goto L_2aa43e0;
  else goto L_149c620;
L_2aa43e0:
  if (x[3] == 1) goto L_2b40ec0;
  else goto L_2aa43c0;
L_2b40ec0:
  if (x[12] == 1) goto L_149c620;
  else goto L_2b40ea0;
L_2b40ea0:
  if (x[2] == 1) goto L_149c620;
  else goto L_2a563c0;
L_2a563c0:
  if (x[11] == 1) goto L_149c620;
  else goto L_2a563a0;
L_2a563a0:
  if (x[1] == 1) goto L_149c620;
  else goto L_2a56380;
L_2aa43c0:
  if (x[12] == 1) goto L_2aa43a0;
  else {
    return_bit = !return_bit;
    goto L_2a352e0;
  }
L_2aa43a0:
  if (x[2] == 1) goto L_2a8cea0;
  else goto L_2aa4380;
L_2a8cea0:
  if (x[11] == 1) goto L_149c620;
  else goto L_149cac0;
L_2aa4380:
  if (x[11] == 1) goto L_2aa4360;
  else goto L_2abe1e0;
L_2aa4360:
  if (x[1] == 1) goto L_149c620;
  else goto L_2abefc0;
L_2abefc0:
  if (x[10] == 1) goto L_149c620;
  else goto L_2b3ae00;
L_2b3ae00:
  if (x[0] == 1) goto L_149c620;
  else {
    return_bit = !return_bit;
    goto L_149cb40;
  }
L_2abe1e0:
  if (x[1] == 1) goto L_149cac0;
  else goto L_2b3c440;
L_2b3c440:
  if (x[10] == 1) goto L_149c620;
  else {
    return_bit = !return_bit;
    goto L_149cb00;
  }
L_2a352e0:
  if (x[2] == 1) goto L_2a41ba0;
  else goto L_2afc1a0;
L_2a41ba0:
  if (x[11] == 1) goto L_149c620;
  else goto L_2afc180;
L_2afc180:
  if (x[1] == 1) goto L_2b0d240;
  else goto L_2ab7f20;
L_2b0d240:
  if (x[10] == 1) goto L_149c620;
  else goto L_2ab7e00;
L_2afc1a0:
  if (x[11] == 1) goto L_2afc180;
  else {
    return_bit = !return_bit;
    goto L_149c620;
  }
L_2aa4940:
  if (x[17] == 1) goto L_2aa46a0;
  else goto L_2aa4920;
L_2aa46a0:
  if (x[7] == 1) goto L_2aa4680;
  else goto L_149c620;
L_2aa4680:
  if (x[16] == 1) goto L_149c620;
  else goto L_2aa4660;
L_2aa4660:
  if (x[6] == 1) goto L_2aa4640;
  else goto L_149c620;
L_2aa4640:
  if (x[15] == 1) goto L_149c620;
  else goto L_2aa4620;
L_2aa4620:
  if (x[5] == 1) goto L_2aa4600;
  else goto L_149c620;
L_2aa4600:
  if (x[14] == 1) goto L_149c620;
  else goto L_2aa45e0;
L_2aa45e0:
  if (x[4] == 1) goto L_2aa45c0;
  else goto L_149c620;
L_2aa45c0:
  if (x[13] == 1) goto L_149c620;
  else goto L_2aa45a0;
L_2aa45a0:
  if (x[3] == 1) goto L_2aa4580;
  else goto L_149c620;
L_2aa4580:
  if (x[12] == 1) goto L_149c620;
  else goto L_2aa4560;
L_2aa4560:
  if (x[2] == 1) goto L_149c620;
  else goto L_2acbe60;
L_2acbe60:
  if (x[11] == 1) goto L_149c620;
  else goto L_2b02a20;
L_2b02a20:
  if (x[1] == 1) goto L_149c620;
  else goto L_2bc4480;
L_2bc4480:
  if (x[10] == 1) goto L_2ad2a20;
  else goto L_149c620;
L_2aa4920:
  if (x[7] == 1) goto L_2aa4900;
  else goto L_149c620;
L_2aa4900:
  if (x[16] == 1) goto L_2aa48e0;
  else goto L_149c620;
L_2aa48e0:
  if (x[6] == 1) goto L_2aa48c0;
  else goto L_149c620;
L_2aa48c0:
  if (x[15] == 1) goto L_2aa48a0;
  else goto L_149c620;
L_2aa48a0:
  if (x[5] == 1) goto L_2aa4880;
  else goto L_149c620;
L_2aa4880:
  if (x[14] == 1) goto L_2aa4860;
  else goto L_149c620;
L_2aa4860:
  if (x[4] == 1) goto L_2aa4840;
  else goto L_149c620;
L_2aa4840:
  if (x[13] == 1) goto L_2aa4820;
  else goto L_149c620;
L_2aa4820:
  if (x[3] == 1) goto L_2aa4800;
  else goto L_149c620;
L_2aa4800:
  if (x[12] == 1) goto L_2aa47a0;
  else goto L_149c620;
L_2aa47a0:
  if (x[2] == 1) goto L_2aa46e0;
  else goto L_2aa4780;
L_2aa46e0:
  if (x[11] == 1) goto L_2aa46c0;
  else goto L_149c620;
L_2aa46c0:
  if (x[10] == 1) goto L_149cb40;
  else {
    return_bit = !return_bit;
    goto L_149cb40;
  }
L_2aa4780:
  if (x[11] == 1) goto L_2aa4700;
  else goto L_149c620;
L_2aa4700:
  if (x[1] == 1) goto L_2aa46c0;
  else goto L_2ac0620;
L_2ac0620:
  if (x[10] == 1) goto L_2afda40;
  else {
    return_bit = !return_bit;
    goto L_2ab7e00;
  }
L_2afda40:
  if (x[0] == 1) goto L_149cb40;
  else goto L_149c620;
entry_point:
  switch(bit) {
    case 0:
      return_bit = 0;
      goto L_2a77001;
    case 1:
      return_bit = 0;
      goto L_2aa4961;
  }
}

