set terminal jpeg
set output "ctrl_reg.goal.gnuplot.jpg"
unset key
set xlabel "x_x1"
set xrange [-3.454000:3.454000]
set ylabel "x_x2"
set yrange [-4.000000:4.000000]
#set grid
set object rectangle from -0.107937 , -0.109375 to 0.107937 , 0.109375 fs solid 0.3 noborder fc rgb "#79F3A1" 
plot -4.000000, 4.000000
