set terminal jpeg
set output "ctrl_reg.strong_ctrl.act.jpg"
unset key
set xrange [-3.454000:3.454000]
set xlabel "w_u"
set yrange [0:1]
set ylabel ""
#set grid
set xtics ("(-1),(1),(0)" -2.878333, "(-1)" -1.727000, "(1),(0)" -0.575667, "(0)" 0.575667, "(1)" 1.727000, "(-1),(0)" 2.878333)
set object rectangle from -3.454000 , 0 to -2.302667 , 1 fs solid 0.3 noborder fc rgb "#79F3CA"
set object rectangle from -2.302667 , 0 to -1.151333 , 1 fs solid 0.3 noborder fc rgb "#AC79F3"
set object rectangle from -1.151333 , 0 to 0.000000 , 1 fs solid 0.3 noborder fc rgb "#EE79F3"
set object rectangle from 0.000000 , 0 to 1.151333 , 1 fs solid 0.3 noborder fc rgb "#79F388"
set object rectangle from 1.151333 , 0 to 2.302667 , 1 fs solid 0.3 noborder fc rgb "#79B1F3"
set object rectangle from 2.302667 , 0 to 3.454000 , 1 fs solid 0.3 noborder fc rgb "#F3798D"
plot -4.000000, 4.000000
