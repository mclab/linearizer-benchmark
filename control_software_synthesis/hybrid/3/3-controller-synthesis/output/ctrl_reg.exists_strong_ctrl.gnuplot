set terminal jpeg
set output "ctrl_reg.exists_strong_ctrl.gnuplot.jpg"
unset key
set xlabel "x_x1"
set xrange [-3.454000:3.454000]
set ylabel "x_x2"
set yrange [-4.000000:4.000000]
#set grid
set object rectangle from -0.121430 , 0.218750 to -0.107938 , 0.250000 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.107937 , 0.031250 to -0.094445 , 0.234375 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.094445 , -0.046875 to -0.080953 , 0.218750 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.080953 , -0.046875 to -0.067461 , 0.203125 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.067461 , -0.046875 to -0.053969 , 0.187500 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.053969 , -0.046875 to -0.040477 , 0.171875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.040477 , -0.046875 to -0.026984 , 0.156250 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.026984 , -0.046875 to -0.013492 , 0.140625 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.013492 , -0.046875 to 0.000000 , 0.015625 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from -0.013492 , 0.031250 to 0.000000 , 0.046875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.000000 , -0.046875 to 0.013492 , -0.031250 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.000000 , -0.015625 to 0.013492 , 0.046875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.013492 , -0.140625 to 0.026984 , 0.046875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.026984 , -0.156250 to 0.040477 , 0.046875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.040477 , -0.171875 to 0.053969 , 0.046875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.053969 , -0.187500 to 0.067461 , 0.046875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.067461 , -0.203125 to 0.080953 , 0.046875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.080953 , -0.218750 to 0.094445 , 0.046875 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.094445 , -0.234375 to 0.107937 , -0.031250 fs solid 0.3 noborder fc rgb "#79F3A1"
set object rectangle from 0.107937 , -0.250000 to 0.121430 , -0.218750 fs solid 0.3 noborder fc rgb "#79F3A1" 
plot -4.000000, 4.000000
