#ifndef __CTRL_H__
#define __CTRL_H__
#define STATE_VARS 2
#define OUTPUT_VARS 2
#define ACTION_VARS 1
#define DIM_X_VECTOR 18
#define DIM_A_VECTOR 2
#define X_X1 0
#define X_X2 1
#define W_U 0
/*
  order for double *x (state) arrays is as follows:
	x[0]: x_x1
	x[1]: x_x2

  order for double *a (action) arrays is as follows:
	a[0]: w_u
*/
int K_with_AD_DA(double *x, double *a);
int exists_K_with_AD(double *x);
void DA(unsigned char *a_quant, double *a);
void AD(double *x, unsigned char *x_quant);
int K_with_AD_DA_expl(double x_x1, double x_x2, int *w_u);
int exists_K_with_AD_expl(double x_x1, double x_x2);
void DA_expl(unsigned char *, int *w_u);
void DA_expl_w_u(unsigned char *, int *);
void AD_expl(double x_x1, double x_x2, unsigned char *);
void AD_expl_x_x1(double, unsigned char *);
void AD_expl_x_x2(double, unsigned char *);
int K(unsigned char *, unsigned char *);
int K_exists(unsigned char *);

#include <stdlib.h>
#include <string.h>

typedef struct var_info {
  double init;
  double lower, upper;
  double delta;
  char *name, is_real;
  unsigned bits; /* number of needed bits */
  char **names; /* names[bits] */
  unsigned *indexes; /* indexes[bits] */
} var_info;

static var_info *init_statevars(void)
{
  var_info *res = (var_info *) calloc(STATE_VARS, sizeof(var_info));
  res[0].delta = 1.349219e-02;
  res[0].lower = -3.454;
  res[0].upper = 3.454;
  res[0].is_real = 1;
  res[0].name = (char *)malloc(5*sizeof(char));
  strcpy(res[0].name, "x_x1");
  res[0].bits = 9;
  res[0].names = (char **) calloc(res[0].bits, sizeof(char *));
  res[0].names[0] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[0], "x_x1.0");
  res[0].names[1] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[1], "x_x1.1");
  res[0].names[2] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[2], "x_x1.2");
  res[0].names[3] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[3], "x_x1.3");
  res[0].names[4] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[4], "x_x1.4");
  res[0].names[5] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[5], "x_x1.5");
  res[0].names[6] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[6], "x_x1.6");
  res[0].names[7] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[7], "x_x1.7");
  res[0].names[8] = (char *)malloc(7*sizeof(char));
  strcpy(res[0].names[8], "x_x1.8");
  res[0].indexes = (unsigned *) calloc(res[0].bits, sizeof(int));
  res[0].indexes[0] = 0;
  res[0].indexes[1] = 1;
  res[0].indexes[2] = 2;
  res[0].indexes[3] = 3;
  res[0].indexes[4] = 4;
  res[0].indexes[5] = 5;
  res[0].indexes[6] = 6;
  res[0].indexes[7] = 7;
  res[0].indexes[8] = 8;
  res[1].delta = 1.562500e-02;
  res[1].lower = -4;
  res[1].upper = 4;
  res[1].is_real = 1;
  res[1].name = (char *)malloc(5*sizeof(char));
  strcpy(res[1].name, "x_x2");
  res[1].bits = 9;
  res[1].names = (char **) calloc(res[1].bits, sizeof(char *));
  res[1].names[0] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[0], "x_x2.0");
  res[1].names[1] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[1], "x_x2.1");
  res[1].names[2] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[2], "x_x2.2");
  res[1].names[3] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[3], "x_x2.3");
  res[1].names[4] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[4], "x_x2.4");
  res[1].names[5] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[5], "x_x2.5");
  res[1].names[6] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[6], "x_x2.6");
  res[1].names[7] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[7], "x_x2.7");
  res[1].names[8] = (char *)malloc(7*sizeof(char));
  strcpy(res[1].names[8], "x_x2.8");
  res[1].indexes = (unsigned *) calloc(res[1].bits, sizeof(int));
  res[1].indexes[0] = 9;
  res[1].indexes[1] = 10;
  res[1].indexes[2] = 11;
  res[1].indexes[3] = 12;
  res[1].indexes[4] = 13;
  res[1].indexes[5] = 14;
  res[1].indexes[6] = 15;
  res[1].indexes[7] = 16;
  res[1].indexes[8] = 17;
  return res;
}

static var_info *init_actionvars(void)
{
  var_info *res = (var_info *) calloc(ACTION_VARS, sizeof(var_info));
  res[0].delta = 1;
  res[0].is_real = 0;
  res[0].lower = -1;
  res[0].upper = 1;
  res[0].name = "w_u";
  res[0].name = (char *)malloc(4*sizeof(char));
  strcpy(res[0].name, "w_u");
  res[0].bits = 2;
  res[0].names = (char **) calloc(res[0].bits, sizeof(char *));
  res[0].names[0] = (char *)malloc(6*sizeof(char));
  strcpy(res[0].names[0], "w_u.0");
  res[0].names[1] = (char *)malloc(6*sizeof(char));
  strcpy(res[0].names[1], "w_u.1");
  res[0].indexes = (unsigned *) calloc(res[0].bits, sizeof(int));
  res[0].indexes[0] = 0;
  res[0].indexes[1] = 1;
  return res;
}

#endif
