function Controller
  input Real angle;
  input Real velocity;
  output Integer u;
  external "C" K_with_AD_DA_expl(angle, velocity, u)
  annotation(
    Include="#include \"ctrl.c\""
  );
end Controller;