model Plant
  import Modelica.Constants.pi;
  import Modelica.Constants.g_n;

  parameter Real l = g_n "Length of the rod";
  parameter Real m = 1 / (l * l) "Mass of the rod";
  parameter Real b = 0.01 "Friction constant";
  parameter Real F = 0.5;

  parameter Real angle_start = pi;
  parameter Real velocity_start = 0;

  output Real angle;
  output Real angle_normalized;
  output Real velocity;

  Boolean below;

  input Integer u;

initial equation
  angle = angle_start;
  velocity = velocity_start;

equation
  angle_normalized = angle - floor((angle + pi) / (2 * pi)) * 2 * pi;
  below = (angle_normalized < -pi /2 ) or (angle_normalized > pi / 2);

  der(angle) = velocity;
  der(velocity) = (g_n / l) * sin(angle) - (if below then (b / (m * l * l)) * velocity else 0) + u * F / (m * l * l);

end Plant;
