model ClosedLoop
  Plant plant;
  parameter Real T = 0.1;
equation
  when sample(0, T) then
    plant.u = Controller(plant.angle_normalized, plant.velocity);
  end when;
end ClosedLoop;