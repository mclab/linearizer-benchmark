set terminal jpeg
set output "ctrl_reg.strong_ctrl.act.jpg"
unset key
set xrange [-3.454000:3.454000]
set xlabel "w_u"
set yrange [0:1]
set ylabel ""
#set grid
set xtics ("(1)" -2.960571, "(-1),(1),(0)" -1.973714, "(1),(0)" -0.986857, "(-1),(1)" -0.000000, "(-1),(0)" 0.986857, "(0)" 1.973714, "(-1)" 2.960571)
set object rectangle from -3.454000 , 0 to -2.467143 , 1 fs solid 0.3 noborder fc rgb "#79B1F3"
set object rectangle from -2.467143 , 0 to -1.480286 , 1 fs solid 0.3 noborder fc rgb "#79F3CA"
set object rectangle from -1.480286 , 0 to -0.493429 , 1 fs solid 0.3 noborder fc rgb "#EE79F3"
set object rectangle from -0.493429 , 0 to 0.493429 , 1 fs solid 0.3 noborder fc rgb "#D4F379"
set object rectangle from 0.493429 , 0 to 1.480286 , 1 fs solid 0.3 noborder fc rgb "#F3798D"
set object rectangle from 1.480286 , 0 to 2.467143 , 1 fs solid 0.3 noborder fc rgb "#79F388"
set object rectangle from 2.467143 , 0 to 3.454000 , 1 fs solid 0.3 noborder fc rgb "#AC79F3"
plot -4.000000, 4.000000
