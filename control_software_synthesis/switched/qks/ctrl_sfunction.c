/**
 * MATLAB 2-levl S-Function for QKS controller
 * Federico Mari <mari@di.uniroma1.it> 2016
 */

#define S_FUNCTION_NAME ctrl_sfunction
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include "ctrl.h" /* QKS output, coming with ctrl.c implementation */


/*====================*
 * S-function methods *
 *====================*/

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    int_T nInputPorts  = 1;  /* number of input ports  */
    int_T nOutputPorts = 1;  /* number of output ports */
    int_T needsInput   = 1;  /* direct feed through    */

    int_T inputPortIdx  = 0;
    int_T outputPortIdx = 0;

    ssSetNumSFcnParams(S, 0);  /* Number of expected parameters */
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 2); /* [x1 x2] */

    if (!ssSetNumInputPorts(S, nInputPorts)) return;
    ssSetInputPortWidth(S, inputPortIdx, 2);
    ssSetInputPortDirectFeedThrough(S, inputPortIdx, needsInput);

    if (!ssSetNumOutputPorts(S, nOutputPorts)) return;
    ssSetOutputPortWidth(S, outputPortIdx, nOutputPorts);

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);
    ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);

    /* Take care when specifying exception free code - see sfuntmpl_doc.c */
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}



/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specify the sample time as 1.0
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, 0.1);
    ssSetOffsetTime(S, 0, 0.0);
    /* ssSetModelReferenceSampleTimeDefaultInheritance(S); I'm not sure... */
}

// #define MDL_INITIALIZE_CONDITIONS
// /* Function: mdlInitializeConditions ========================================
//  * Abstract:
//  *    Initialize both discrete states to one.
//  */
// static void mdlInitializeConditions(SimStruct *S)
// {
//     // real_T *x0 = ssGetRealDiscStates(S);
//     // int_T  lp;
//     //
//     // for (lp=0;lp<2;lp++) {
//     //     *x0++=1.0;
//     // }
// }



/* Function: mdlOutputs =======================================================
 * Abstract:
 *      K_with_AD_DA(U, y)
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    real_T            *u    = ssGetOutputPortSignal(S, 0);
    // real_T            *x    = ssGetDiscStates(S);
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S, 0);
    real_T            x[2];

    x[0] = *uPtrs[0];
    x[1] = *uPtrs[1];

    UNUSED_ARG(tid); /* not used in single tasking mode */

    ssPrintf("t=%f\tx[1]=%f\tx[2]=%f\n", ssGetT(S), x[0], x[1]);

    if (K_with_AD_DA(x, u)) {
        ssSetErrorStatus(S, "Controller not defined on current signal");
        return;
    };
    u[0] = u[0]*0.5;
    ssPrintf("u = %f\n", u[0]);
}



/* Function: mdlTerminate =====================================================
 * Abstract:
 *    No termination needed, but we are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
    UNUSED_ARG(S); /* unused input argument */
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
