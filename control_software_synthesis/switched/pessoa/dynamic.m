%% Inverted Pendulum with Fixed Pivot Point
%  Federico Mari
%  CS Dept, Sapienza Univ of Rome, January 2017
%  Example to be put in "Linearizing Discrete Time Hybrid Systems" TAC
%
%   DYNAMICS SCRIPT
%

function dx = dynamic(t,x)
global u;

% Physical parameters

g = 9.8;
l = g;
m = 1.0 / (l*l);
b = 0.01;

% Nonlinear Dynamics

dx(1,1) = x(2);
dx(2,1) = (g/l) * sin(x(1)) - (b/(m*l*l)) * x(2) + (1/(m*l*l)) * u;
