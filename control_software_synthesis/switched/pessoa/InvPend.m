%% Inverted Pendulum with Fixed Pivot Point
%  Federico Mari
%  CS Dept, Sapienza Univ of Rome, January 2017
%  Example to be put in "Linearizing Discrete Time Hybrid Systems" TAC
%
%    MAIN SCRIPT
%

clear all;
close all;
clc;
global u;

addpath('/home/mari/exps/tac/pessoa/Pessoa/Pessoa_1.4_GLNX64/');
warning('off','all');

%% System definition

invpend_cont.custom = 1;


%% Abstraction parameters

%  With QKS we use 9 bits for each variable.
%  For this reason, our control abstraction has (2**9)*(2**9) = (2**18) states.
%  In order to have a fair comparison we set eta s.t. we get the same
%  number of states.
%  In particular, x1 range has length 2pi and x2 range has length 8.
%  We want eta s.t. 2pi/eta * 8/eta = 2**18, having
%  eta = sqrt((2pi * 8)/(2**18)) = 0.0138.
%  Actually, the number of states obtained with eta = 0.138 is slightly greater
%  than 2**18 but comparable.

invpend_symb.tau  = 0.1;                % Time quantization with replication
invpend_symb.eta  = 0.0138;             % State quantization
invpend_symb.mu   = 0.5;                % Input quantization
invpend_symb.xset = [-1.1*pi 1.1*pi; -4 4];     % Range of values for the states
                                        % in the symbolic model.
invpend_symb.uset = [-0.5 0.5];         % Range of values for the input
                                        % in the symbolic model.
invpend_symb.epsilon = 0.1;               % Model resolution

%% Abstract

% The variable invpend_name records the name of the file where the
% symbolic model will be stored.

invpend_name = 'InvPend';

% The Relation flag controls what kind of relation is to be established
% between the original model and the symbolic model:
%       Relation=1 creates a symbolic model related to the original model
% by an approximate bisimulation relation (deterministic).
%       Relation=2 creates a symbolic model related to the original model
% by an approximate alternating simulation relation (non-deterministic).

Relation = 1;

% The BatchSize flag controls the size of the computation batches.
% Transitions are generated and added to the symbolic model in batches of
% size BatchSize. Different values for BatchSize affect the computation
% time of the symbolic model. Typical values range between 100 and 1000.

BatchSize = 1000;

% The Verbose flag lets you control how much information is displayed while
% the symbolic model is being computed:
%       Verbose=0 provides no information.
%       Verbose=1 provides messages printed on the screen.
%       Verbose=2 provides a graphical representation for the symbolic
% model in addition to the messages printed when the flag is set to 1.
% This flag is only available for 2-dimensional systems.
%       Verbose=3 debug mode

Verbose = 1;

% The command pss_abstract computes the symbolic model.

pss_abstract(invpend_cont, invpend_symb, invpend_name, Relation, BatchSize, Verbose)

%% Synthesize controller

% Verbose flag. Same as before.
Verbose = 1;

% BatchSize flag. Same as before.
BatchSize = 1000;

% The parameter TargetSet_name records the name of the file where the
% target set will be stored.

TargetSet_name{1} = 'InvPendTargetSet';

% The target set can be defined by a rectangle (faster) or by defining the
% characteristic function of the desired target set (slower). The parameter
% TargetSet is used to specify the target set rectangle. To define the
% target set through its characteristic function, please edit the file
% pss_target_set. When using the characteristic function method, it is
% assumed that the parameter TargetSet is an over-approximation of the
% the target set.

% QKS Goal
%   -0.1 <= x1  &  x1 <= 0.1;
%   -0.1 <= x2  &  x2 <= 0.1;

TargetSet = [-0.1 0.1; -0.1 0.1];

% The flag TargetSetType determines if a rectangle or a characteristic
% function is to be used:
%       TargetSetType=0: rectangle.
%       TargetSetType=1: characteristic function.

TargetSetType = 0;

% Build the target set

pss_build_set(invpend_name, TargetSet, TargetSet_name{1}, BatchSize, TargetSetType, Verbose)

% The parameter DCMotorController_name records the name of the file where
% the symbolic controller will be stored.

invpend_Controller_name = 'InvPendController';

% The flag ControlObjective specifies the control objective as a function
% of two auxiliary sets: the target set W and the constraint set Z.
%       ControlObjective=1: remain within the target set (always W).
%       ControlObjective=2: reach the target set (eventually W).
%       ControlObjective=3: reach and remain within the target set
% (eventually always W).
%       ControlObjective=4: reach and remain within the target set while
% always remaining within the constraint set (eventually always W and
% always Z).

ControlObjective = 3;

% Synthesize the controller

pss_design(invpend_name, TargetSet_name, invpend_Controller_name, ControlObjective, BatchSize, Verbose)

%% Simulate closed-loop
xini = [-0.6; 0];
uini = [0];
eta  = invpend_symb.eta;
tau  = invpend_symb.tau;
contr_name = invpend_Controller_name;
% switched_pendulum1;
