function xend=pss_dynamics(params_symb, params_cont, xl, ul)
global u;

%   xend = pss_dynamics(params_symb, params_cont, xl, ul)
%
%   This function computes the transitions in labels (as opposed to coordinates) directly.
%	In general after the simulation step one would need to call "pss_build_fsm_indices.m"
%   at the end to return labels.
%
%       INPUTS: xl - state labels.
%           	ul - input labels.
%
%       OUTPUT: xend	- ending state(s) in labels.
%
%   Majid Zamani <zamani@ee.ucla.edu>, CyPhyLab-UCLA 2009.

%% Transform labels into states (I'll probably make a function for this like the one at the end)

u = params_symb.mu*(ul+params_symb.min(params_symb.uind));
xini = params_symb.eta*(xl+params_symb.min(params_symb.xoind));

%% Here computations on states

tau=params_symb.tau;
eta=params_symb.eta;
muh=params_symb.mu/2;
etah=eta/2;

radius=3.6482*exp(1.1433*tau)*etah;

% Compute the center of the ball containing the reachable set

% Discretization step = tau/10
[t,xout]=ode45(@dynamic, [0:tau/10:tau], xini);

% % Normalization of x1 in [-pi, pi] (hard-coded!)
for c=1:length(xout)
    while (xout(c,1) < -pi)
        xout(c,1) = xout(c,1) + 2*pi;
    end
    while (xout(c,1) > pi)
        xout(c,1) = xout(c,1) - 2*pi;
    end
end

xpcenter=[xout(end,1);xout(end,2)];
maxpoint=xpcenter+radius*ones(2,1);
minpoint=xpcenter-radius*ones(2,1);

%% Label computations

maxpoint_label=zeros(2,1);
minpoint_label=zeros(2,1);

for k=1:2
    if (mod(maxpoint(k),eta)~=etah)
        maxpoint_label(k)=round(maxpoint(k)/eta)-params_symb.min(k);
    else
        maxpoint_label(k)=floor(maxpoint(k)/eta)-params_symb.min(k);
    end

    if (mod(minpoint(k),eta)~=etah)
        minpoint_label(k)=round(minpoint(k)/eta)-params_symb.min(k);
    else
        minpoint_label(k)=ceil(minpoint(k)/eta)-params_symb.min(k);
    end
end

%% Add all the points in the Post
% Some check needed to wrap around in the 3rd coordinate

xend=[];
x=minpoint_label;
ntrans=1;
jl=1;
while(jl<=2 && x(2)<=maxpoint_label(2))
jl=1;
    xend=[xend x];
    while(jl<2 && x(jl)>=maxpoint_label(jl))
        x(jl)=minpoint_label(jl);
        jl=jl+1;
    end
    x(jl)=x(jl)+1;
    ntrans=ntrans+1;
end
