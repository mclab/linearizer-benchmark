#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

#include <argtable2.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <glpk.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "common.h"

#define SQR(x) ((x) * (x))

struct arg_dbl *a_width;
struct arg_dbl *a_eps;
struct arg_int *a_m;
struct arg_file *a_output;
struct arg_end *a_end;
struct arg_lit *a_h;
struct arg_end *a_h_end;

double
f(double *x) {
  return x[1] * cos(x[0]);
}

double
lipschitz_f(gsl_vector const *a, gsl_vector const *b) {
  double b1 = 1;
  double b2 = fmax(fabs(gsl_vector_get(a, 1)), fabs(gsl_vector_get(b, 1)));
  return sqrt(SQR(b1) + SQR(b2));
}

void
print_usage(char const *name, void **argtable, void **argtable_h) {
  fprintf(stderr, "\nLinearisation of the function y*cos(x) where x ranges in [0,2*pi]\n\n");
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "\t%s: ", name);
  arg_print_syntaxv(stderr, argtable, "\n");
  fprintf(stderr, "\t%s: ", name);
  arg_print_syntaxv(stderr, argtable_h, "\n");
  fprintf(stderr, "\n");
  arg_print_glossary(stderr, argtable, "%-25s %s\n");
  arg_print_glossary(stderr, argtable_h, "%-25s %s\n");
  fprintf(stderr, "\n");
}

int
main(int argc, char *argv[]) {
  glp_term_out(GLP_OFF);
  void *argtable[] = {
    a_width = arg_dbl1(NULL, "alpha", "<val>", "y ranges in [-alpha,alpha]"),
    a_eps = arg_dbl1(NULL, "eps", "<val>", "error threshold"),
    a_m = arg_int1("m", NULL, "<num>", "number of sampling points"),
    a_output = arg_file1("o", NULL, "<file>", "output file"),
    a_end = arg_end(20)
  };
  void *argtable_h[] = {
    a_h = arg_lit1("h", "help", "print help message"),
    a_h_end = arg_end(20)
  };
  int nerrors = arg_parse(argc, argv, argtable);
  int nerrors_h = arg_parse(argc, argv, argtable_h);
  if (nerrors == 0) {
    double w = a_width->dval[0];
    double eps = a_eps->dval[0];
    size_t m = a_m->ival[0];
    gsl_vector *a = gsl_vector_alloc(2);
    gsl_vector_set(a, 0, 0);
    gsl_vector_set(a, 1, -w);
    gsl_vector *b = gsl_vector_alloc(2);
    gsl_vector_set(b, 0, 2 * M_PI);
    gsl_vector_set(b, 1, w);
    volume_total = 4 * M_PI * w;
    num_dimensions = 2;
    LPSolver *solver = lpsolver_new(LPSOLVER_BACKEND_TYPE_GLPK);
    print_header();
    time_start = clock();
    Linearization *lin = linearization_new(f, 2, lipschitz_f, a, b, eps, m, solver, &callback);
    print_stats(100, lin_cover_get_num(lin->cover), get_cpu_time(), get_memory_peak());
    FILE *stream = fopen(a_output->filename[0], "w");
    linearization_fprintf(stream, lin);
    fclose(stream);
    linearization_destroy(&lin);
    lpsolver_destroy(&solver);
    gsl_vector_free(a);
    gsl_vector_free(b);
  }
  else if (nerrors_h == 0) {
    print_usage(argv[0], argtable, argtable_h);
    exit(EXIT_SUCCESS);
  }
  else {
    arg_print_errors(stderr, a_end, argv[0]);
    fprintf(stderr, "\n");
    print_usage(argv[0], argtable, argtable_h);
    exit(EXIT_FAILURE);
  }
  arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
  arg_freetable(argtable_h,sizeof(argtable_h)/sizeof(argtable_h[0]));
  exit(EXIT_SUCCESS);
}
