#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_vector.h>

double
volume_total;
size_t
num_dimensions;
clock_t
time_start;

void
print_header() {
  fprintf(stdout, "%12s%8s%12s%12s\n", "Progress", "n", "CPU (sec)", "RAM (KB)");
}

void
print_stats(double progress, size_t num, double cpu, unsigned long ram) {
  fprintf(stdout, "%11.2f%%%8zu%12.3g%12lu\n", progress, num, cpu, ram);
  fflush(stdout);
}

double
get_cpu_time() {
  clock_t time_end = clock();
  return ((double) (time_end - time_start)) / CLOCKS_PER_SEC;
}

unsigned long
get_memory_peak() {
  unsigned long res = 0;
  FILE *s = fopen("/proc/self/status", "r");
  char buffer[100]; 
  for (;;) {
    fgets(buffer, 100, s);
    if (strncmp("VmPeak", buffer, 6) == 0) {
      break;
    }
  }
  fclose(s);
  sscanf(buffer, "VmPeak: %lu kB", &res);
  return res;
}

void
callback(gsl_vector const *a, gsl_vector const *b) {
  static size_t num_intervals = 0;
  static double volume_done = 0;
  ++num_intervals;
  double volume_add = 1;
  for (size_t j = 0; j < num_dimensions; ++j) {
    volume_add *= (gsl_vector_get(b, j) - gsl_vector_get(a, j));
  }
  volume_done += volume_add;
  double progress = 100 * volume_done / volume_total;
  static unsigned int k = 0;
  double percentage[] = {0.1, 0.15, 0.2, 0.5, 1.0, 1.5, 2.0, 5.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 95.0};
  size_t const PRINT_NUM = sizeof(percentage) / sizeof(double);
  if ((k < PRINT_NUM) && (progress > percentage[k])) {
    print_stats(percentage[k], num_intervals, get_cpu_time(), get_memory_peak());
    while ((k < PRINT_NUM) && (progress > percentage[k])) {
      ++k;
    }
  }
}
