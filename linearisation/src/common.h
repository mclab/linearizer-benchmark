#ifndef CALLBACK_H_
#define CALLBACK_H_

extern void
callback(gsl_vector const * /*a*/, gsl_vector const * /*b*/);

extern void
print_header();

extern void
print_stats(double /*progress*/, size_t /*num*/, double /*cpu*/, unsigned long /*ram*/);

extern double
get_cpu_time();

extern unsigned long
get_memory_peak();

extern double
volume_total;
extern size_t
num_dimensions;
extern clock_t
time_start;

#endif /* #ifndef CALLBACK_H_ */
