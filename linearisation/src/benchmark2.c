#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

#include <argtable2.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <glpk.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "common.h"

#define SQR(x) (x) * (x)

double
f(double *x, size_t n) {
  double res = 0;
  for (size_t i = 0; i < n; ++i) {
    res += x[i] * x[i + 1];
  }
  return res;
}

#define F(n) double f##n(double *x) { return f(x, n); }

double
lipschitz_f(size_t n, gsl_vector const *a, gsl_vector const *b) {
  double first = GSL_MAX(fabs(gsl_vector_get(a, 0)), fabs(gsl_vector_get(b, 0)));
  double last = GSL_MAX(fabs(gsl_vector_get(a, n)), fabs(gsl_vector_get(b, n)));
  double sum = 0;
  double term = 0;
  for (size_t i = 0; i < n - 1; ++i) {
    term = GSL_MAX(fabs(gsl_vector_get(a, i) + gsl_vector_get(a, i + 2)), fabs(gsl_vector_get(b, i) + gsl_vector_get(b, i + 2)));
    sum += SQR(term);
  }
  return sqrt(SQR(first) + sum + SQR(last));
}

#define LIPSCHITZ_F(n) double lipschitz_f##n(gsl_vector const *a, gsl_vector const *b) { return lipschitz_f(n, a, b); }

F(1)
F(2)
F(3)
F(4)
FuncPtr fs[] = { f1, f2, f3, f4 };

LIPSCHITZ_F(1)
LIPSCHITZ_F(2)
LIPSCHITZ_F(3)
LIPSCHITZ_F(4)
LipschitzFuncPtr lipschitz_fs[] = { lipschitz_f1, lipschitz_f2, lipschitz_f3, lipschitz_f4 };

struct arg_int *a_n;
struct arg_dbl *a_width;
struct arg_dbl *a_eps;
struct arg_int *a_m;
struct arg_file *a_output;
struct arg_lit *a_term_wise;
struct arg_end *a_end;
struct arg_lit *a_h;
struct arg_end *a_h_end;

static size_t
do_the_job(size_t /*n*/, double /*w*/, double /*eps*/, size_t /*m*/, char const * /*fname*/);

static void
print_usage(char const * /*name*/, void ** /*argtable*/, void ** /*argtable_h*/);

int
main(int argc, char *argv[]) {
  glp_term_out(GLP_OFF);
  void *argtable[] = {
    a_n = arg_int1("t", NULL, "<num>", "number of terms (1..4)"),
    a_width = arg_dbl1("w", NULL, "<val>", "x_{i} ranges in [-w,w]"),
    a_eps = arg_dbl1(NULL, "eps", "<val>", "error threshold"),
    a_m = arg_int1("m", NULL, "<num>", "number of sampling points"),
    a_output = arg_file1("o", NULL, "<file>", "output file"),
    a_term_wise = arg_lit0(NULL, "term-wise", "use term-wise approach"),
    a_end = arg_end(20)
  };
  void *argtable_h[] = {
    a_h = arg_lit1("h", "help", "print help message"),
    a_h_end = arg_end(20)
  };
  int nerrors = arg_parse(argc, argv, argtable);
  int nerrors_h = arg_parse(argc, argv, argtable_h);
  if (nerrors == 0) {
    if ((a_n->ival[0] < 1) || (a_n->ival[0] > 4)) {
      fprintf(stderr, "Number of terms must be in 1..4\n");
      exit(EXIT_FAILURE);
    }
    size_t t = a_n->ival[0];
    double w = a_width->dval[0];
    double eps = a_eps->dval[0];
    size_t m = a_m->ival[0];
    char const *fname = a_output->filename[0];
    print_header();
    time_start = clock();
    size_t n = 0;
    if (a_term_wise->count > 0) {
      volume_total = t * SQR(2 * w);
      size_t fname_len = strlen(fname);
      char *fname_current = malloc((fname_len + 2) * sizeof(char));
      strcpy(fname_current, fname);
      for (size_t i = 0; i < t; ++i) {
        sprintf(fname_current + fname_len, "%zu", i + 1);
        n += do_the_job(1, w, eps / t, m, fname_current);
      }
      free(fname_current);
    }
    else {
      volume_total = pow(2 * w, t + 1);
      n = do_the_job(t, w, eps, m, fname);
    }
    print_stats(100, n, get_cpu_time(), get_memory_peak());
  }
  else if (nerrors_h == 0) {
    print_usage(argv[0], argtable, argtable_h);
    exit(EXIT_SUCCESS);
  }
  else {
    arg_print_errors(stderr, a_end, argv[0]);
    fprintf(stderr, "\n");
    print_usage(argv[0], argtable, argtable_h);
    exit(EXIT_FAILURE);
  }
  arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
  arg_freetable(argtable_h,sizeof(argtable_h)/sizeof(argtable_h[0]));
  exit(EXIT_SUCCESS);
}

static void
print_usage(char const *name, void **argtable, void **argtable_h) {
  fprintf(stderr, "\nLinearisation of the function sum x_{i} * x_{i+1}\n\n");
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "\t%s: ", name);
  arg_print_syntaxv(stderr, argtable, "\n");
  fprintf(stderr, "\t%s: ", name);
  arg_print_syntaxv(stderr, argtable_h, "\n");
  fprintf(stderr, "\n");
  arg_print_glossary(stderr, argtable, "%-25s %s\n");
  arg_print_glossary(stderr, argtable_h, "%-25s %s\n");
  fprintf(stderr, "\n");
}

static size_t
do_the_job(size_t t, double w, double eps, size_t m, char const *fname) {
  size_t res;
  num_dimensions = t + 1;
  gsl_vector *a = gsl_vector_alloc(num_dimensions);
  gsl_vector *b = gsl_vector_alloc(num_dimensions);
  for (size_t j = 0; j < t + 1; ++j) {
    gsl_vector_set(a, j, -w);
    gsl_vector_set(b, j, w);
  }
  LPSolver *solver = lpsolver_new(LPSOLVER_BACKEND_TYPE_GLPK);
  Linearization *lin = linearization_new(fs[t - 1], t + 1, lipschitz_fs[t - 1], a, b, eps, m, solver, &callback);
  FILE *stream = fopen(fname, "w");
  linearization_fprintf(stream, lin);
  fclose(stream);
  res = lin_cover_get_num(lin->cover);
  linearization_destroy(&lin);
  lpsolver_destroy(&solver);
  gsl_vector_free(a);
  gsl_vector_free(b);
  return res;
}
