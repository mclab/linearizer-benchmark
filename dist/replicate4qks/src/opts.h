#ifndef LIN_OPTS_H_
#define LIN_OPTS_H_

#define LIN_OPTS_DEF_INPUT_FILE_PATH NULL
#define LIN_OPTS_DEF_OUTPUT_FILE_PATH NULL
#define LIN_OPTS_DEF_BOUNDS_FILE_PATH "computed_bounds.txt"
#define LIN_OPTS_DEF_NUM 10
#define LIN_OPTS_DEF_VERBOSITY 0

extern void
lin_opts_init();

extern void
lin_opts_free();

extern void
lin_opts_print();

extern const char *
lin_opts_get_input_file_path();
extern void
lin_opts_set_input_file_path(const char * /*value*/);

extern const char *
lin_opts_get_output_file_path();
extern void
lin_opts_set_output_file_path(const char * /*value*/);

extern const char *
lin_opts_get_bounds_file_path();
extern void
lin_opts_set_bounds_file_path(const char * /*value*/);

extern unsigned int
lin_opts_get_num();
extern void
lin_opts_set_num(unsigned int /*value*/);

extern unsigned int
lin_opts_get_verbosity();
extern void
lin_opts_set_verbosity(unsigned int /*value*/);

#endif /* LIN_OPTS_H_ */
