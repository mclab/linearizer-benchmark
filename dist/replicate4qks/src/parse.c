/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "src/parse.y" /* yacc.c:339  */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include <lpsolver.h>
#include <linearizer.h>

#include <gmp.h>

#include <utarray.h>

#include "node.h"

#include "errors.h"
#include "memory.h"
#include "utils.h"
#include "hashtable.h"
#include "opts.h"
#include "keywords.h"
#include "node_helpers.h"
#include "output.h"
#include "bounds.h"

extern int
yylex();
extern int
yyparse();
extern void
yyerror(char * /*s*/);

static node *
lin_handle_quantized_formula(Lin_keyword_id /*keyword_id*/, node * /*local_vars_list*/, node * /*formula_list*/);

static node *
lin_handle_constraint(node * /*constraint*/);

static node *
lin_get_var_type(const char * /*name*/);

static node *
lin_create_var(const char * /*name*/, Lin_keyword_id /*type*/, double /*lb*/, double /*ub*/);

static node *
lin_handle_formula(node * /*formula*/);

extern int yylineno;
extern FILE *yyin;

static Lin_hashtable *name_table;
static Lin_hashtable *type_table;
static Lin_hashtable *vars_table;
static Lin_hashtable *local_vars_table;

static bool names_and_types;
static FILE *out;
static node *exists_local_vars;

#line 130 "src/parse.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parse.h".  */
#ifndef YY_YY_SRC_PARSE_H_INCLUDED
# define YY_YY_SRC_PARSE_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    LBRACKET = 258,
    RBRACKET = 259,
    LBRACE = 260,
    RBRACE = 261,
    LPAREN = 262,
    RPAREN = 263,
    NUMBER = 264,
    REALNUMBER = 265,
    CONST = 266,
    TYPE = 267,
    STATEVARS = 268,
    INPUTVARS = 269,
    OUTPUTVARS = 270,
    TRANS = 271,
    SAFETY = 272,
    CONTROLLABLEREGION = 273,
    GOAL = 274,
    OBSERVATION = 275,
    FUNCTIONS = 276,
    FORALL = 277,
    EXISTS = 278,
    ENUM = 279,
    TRUE = 280,
    FALSE = 281,
    DOTDOT = 282,
    SEMICOLON = 283,
    COLON = 284,
    COMMA = 285,
    GEQ = 286,
    GT = 287,
    LEQ = 288,
    LT = 289,
    EQ = 290,
    ID = 291,
    IMPLY = 292,
    OR = 293,
    XOR = 294,
    AND = 295,
    NOT = 296,
    PLUS = 297,
    MINUS = 298,
    TIMES = 299,
    DIVIDE = 300,
    MOD = 301,
    UMINUS = 302
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 76 "src/parse.y" /* yacc.c:355  */

  char *string;
  node *node_ptr;

#line 223 "src/parse.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SRC_PARSE_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 238 "src/parse.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   261

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  48
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  27
/* YYNRULES -- Number of rules.  */
#define YYNRULES  65
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  153

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   302

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   105,   105,   108,   115,   115,   124,   125,   128,   129,
     132,   142,   143,   146,   157,   167,   198,   202,   211,   219,
     225,   229,   233,   249,   253,   257,   261,   265,   269,   273,
     279,   283,   289,   293,   299,   303,   308,   313,   317,   323,
     327,   333,   337,   343,   347,   353,   357,   361,   365,   369,
     373,   377,   381,   385,   389,   395,   399,   403,   407,   411,
     417,   423,   429,   435,   452,   456
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "LBRACKET", "RBRACKET", "LBRACE",
  "RBRACE", "LPAREN", "RPAREN", "NUMBER", "REALNUMBER", "CONST", "TYPE",
  "STATEVARS", "INPUTVARS", "OUTPUTVARS", "TRANS", "SAFETY",
  "CONTROLLABLEREGION", "GOAL", "OBSERVATION", "FUNCTIONS", "FORALL",
  "EXISTS", "ENUM", "TRUE", "FALSE", "DOTDOT", "SEMICOLON", "COLON",
  "COMMA", "GEQ", "GT", "LEQ", "LT", "EQ", "ID", "IMPLY", "OR", "XOR",
  "AND", "NOT", "PLUS", "MINUS", "TIMES", "DIVIDE", "MOD", "UMINUS",
  "$accept", "starting", "model", "const_and_types", "$@1", "const_decl",
  "const_decl_list", "const_decl_atom", "type_decl", "type_decl_list",
  "type_decl_atom", "type", "expr", "expr_list", "trans_spec",
  "safety_spec", "controlregion_spec", "goal_spec", "observation_spec",
  "formula_list", "formula", "linear_constraint", "statevars_decl",
  "inputvars_decl", "outputvars_decl", "local_var", "local_vars_list", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302
};
# endif

#define YYPACT_NINF -93

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-93)))

#define YYTABLE_NINF -17

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-17)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -93,     5,   -93,     7,    10,   -93,   -13,    20,     3,    17,
      14,   -93,   -13,   -13,    29,    18,   -93,     3,   -13,   -93,
      60,   -93,   -93,   -13,    32,    -3,   -93,   -93,    -3,    -3,
     -93,   -93,    57,    -2,    -3,    37,   183,   -93,    88,    55,
     -93,   176,   -28,   138,    -3,   -93,   -93,    -3,    -3,    -3,
      -3,    -3,    -3,   162,   162,    41,   -93,   -93,   162,   199,
     -93,   212,   -93,   116,    61,   -93,    -3,   -93,   193,    72,
     211,   -36,   -36,   -93,   -93,   -93,   169,    74,   177,    52,
      62,    85,   -93,    -3,    -3,    -3,    -3,    -3,   162,   162,
     162,   162,   162,    41,   -93,   125,    75,    15,    -3,   -93,
     -93,   -93,    60,    41,   162,   211,   211,   211,   211,   211,
     -93,   221,    51,    51,   -93,    94,    41,   -93,   153,    -7,
     -93,   -93,   -93,   -93,    95,   162,    97,    41,   -93,   162,
      87,   -93,    84,   109,   162,   117,   -93,    41,   -93,   108,
     131,   162,   135,   -93,   119,   137,   162,   -93,   121,   139,
     -93,   126,   -93
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       4,     0,     2,     0,     7,     1,     0,     0,     0,    12,
       0,    60,    14,     0,     0,     0,     6,     9,     0,     5,
       0,    13,    61,     0,     0,     0,     8,    11,     0,     0,
      20,    21,     0,    22,     0,     0,     0,    62,     0,    36,
      22,     0,     0,     0,     0,    28,    15,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    45,    46,     0,     0,
      33,     0,    47,     0,     0,    10,     0,    29,    31,     0,
      17,    23,    24,    25,    26,    27,     0,     0,     0,     0,
      65,     0,    50,     0,     0,     0,     0,     0,    44,     0,
       0,     0,     0,     0,    35,     0,     0,     0,     0,    19,
      48,    49,     0,     0,     0,    55,    56,    57,    58,    59,
      43,    54,    52,    53,    51,     0,     0,    38,     0,     0,
      18,    30,    63,    64,     0,     0,     0,     0,    40,     0,
       0,     3,     0,     0,     0,     0,    42,     0,    32,     0,
       0,     0,     0,    34,     0,     0,     0,    37,     0,     0,
      39,     0,    41
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -93,   -93,   -93,   -93,   -93,   -93,   127,   -93,   -93,   115,
     -93,    53,     2,    58,   -93,   -93,   -93,   -93,   -93,   -63,
     -16,   -93,   -93,   -93,   -93,   -93,   -92
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,     3,     4,     9,    16,    17,    19,    11,
      12,    35,    59,    69,    39,    64,    96,   119,   131,    60,
      61,    62,     7,    14,    24,    80,    81
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      94,   115,    66,   -16,    29,     5,    30,    31,    50,    51,
      52,   123,   129,   130,    48,    49,    50,    51,    52,   120,
       6,     8,    36,    10,   126,   110,   -16,    41,   -16,    18,
      42,    43,   117,    40,    13,   135,    45,    76,    78,    15,
      34,   124,    82,    20,    23,   142,    68,    25,    38,    70,
      71,    72,    73,    74,    75,   128,    77,    48,    49,    50,
      51,    52,   133,    28,    44,    46,   136,    29,    97,    30,
      31,   140,    63,   111,   112,   113,   114,    79,   145,    95,
      99,   102,    67,   149,    32,   105,   106,   107,   108,   109,
     104,    92,   103,    53,   118,    54,    33,    30,    31,   125,
      68,   132,   134,    34,    36,    83,    84,    85,    86,    87,
     137,    55,   138,    56,    57,   139,    48,    49,    50,    51,
      52,    53,   141,    54,    40,    30,    31,    21,    22,    58,
      53,    34,    54,    27,    30,    31,   143,   144,    37,    93,
     146,    56,    57,   148,    26,   151,    67,   147,   116,   150,
      56,    57,    40,     0,   152,   122,   121,    58,    53,    34,
      54,    40,    30,    31,     0,     0,    58,    53,    34,    54,
       0,    30,    31,     0,     0,   100,   127,     0,    56,    57,
      48,    49,    50,    51,    52,   101,     0,    56,    57,    40,
       0,     0,     0,     0,    58,     0,    34,     0,    40,     0,
       0,     0,     0,    58,    65,    34,    89,    90,    91,    92,
      47,     0,     0,     0,    89,    90,    91,    92,    48,    49,
      50,    51,    52,    98,     0,    48,    49,    50,    51,    52,
      83,    84,    85,    86,    87,    48,    49,    50,    51,    52,
      88,    48,    49,    50,    51,    52,     0,     0,     0,    89,
      90,    91,    92,    48,    49,    50,    51,    52,   -17,    90,
      91,    92
};

static const yytype_int16 yycheck[] =
{
      63,    93,    30,     5,     7,     0,     9,    10,    44,    45,
      46,   103,    19,    20,    42,    43,    44,    45,    46,     4,
      13,    11,    20,    36,   116,    88,    28,    25,    30,    12,
      28,    29,    95,    36,    14,   127,    34,    53,    54,    36,
      43,   104,    58,    29,    15,   137,    44,    29,    16,    47,
      48,    49,    50,    51,    52,   118,    54,    42,    43,    44,
      45,    46,   125,     3,     7,    28,   129,     7,    66,     9,
      10,   134,    17,    89,    90,    91,    92,    36,   141,    18,
       8,    29,     8,   146,    24,    83,    84,    85,    86,    87,
       5,    40,    30,     5,    19,     7,    36,     9,    10,     5,
      98,     6,     5,    43,   102,    31,    32,    33,    34,    35,
      23,    23,    28,    25,    26,     6,    42,    43,    44,    45,
      46,     5,     5,     7,    36,     9,    10,    12,    13,    41,
       5,    43,     7,    18,     9,    10,    28,     6,    23,    23,
       5,    25,    26,     6,    17,     6,     8,    28,    23,    28,
      25,    26,    36,    -1,    28,   102,    98,    41,     5,    43,
       7,    36,     9,    10,    -1,    -1,    41,     5,    43,     7,
      -1,     9,    10,    -1,    -1,     6,    23,    -1,    25,    26,
      42,    43,    44,    45,    46,     8,    -1,    25,    26,    36,
      -1,    -1,    -1,    -1,    41,    -1,    43,    -1,    36,    -1,
      -1,    -1,    -1,    41,    28,    43,    37,    38,    39,    40,
      27,    -1,    -1,    -1,    37,    38,    39,    40,    42,    43,
      44,    45,    46,    30,    -1,    42,    43,    44,    45,    46,
      31,    32,    33,    34,    35,    42,    43,    44,    45,    46,
      28,    42,    43,    44,    45,    46,    -1,    -1,    -1,    37,
      38,    39,    40,    42,    43,    44,    45,    46,    37,    38,
      39,    40
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    49,    50,    51,    52,     0,    13,    70,    11,    53,
      36,    57,    58,    14,    71,    36,    54,    55,    12,    56,
      29,    57,    57,    15,    72,    29,    54,    57,     3,     7,
       9,    10,    24,    36,    43,    59,    60,    57,    16,    62,
      36,    60,    60,    60,     7,    60,    28,    27,    42,    43,
      44,    45,    46,     5,     7,    23,    25,    26,    41,    60,
      67,    68,    69,    17,    63,    28,    30,     8,    60,    61,
      60,    60,    60,    60,    60,    60,    68,    60,    68,    36,
      73,    74,    68,    31,    32,    33,    34,    35,    28,    37,
      38,    39,    40,    23,    67,    18,    64,    60,    30,     8,
       6,     8,    29,    30,     5,    60,    60,    60,    60,    60,
      67,    68,    68,    68,    68,    74,    23,    67,    19,    65,
       4,    61,    59,    74,    67,     5,    74,    23,    67,    19,
      20,    66,     6,    67,     5,    74,    67,    23,    28,     6,
      67,     5,    74,    28,     6,    67,     5,    28,     6,    67,
      28,     6,    28
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    48,    49,    50,    52,    51,    53,    53,    54,    54,
      55,    56,    56,    57,    57,    58,    59,    59,    59,    59,
      60,    60,    60,    60,    60,    60,    60,    60,    60,    60,
      61,    61,    62,    62,    63,    63,    63,    64,    64,    65,
      65,    66,    66,    67,    67,    68,    68,    68,    68,    68,
      68,    68,    68,    68,    68,    69,    69,    69,    69,    69,
      70,    71,    72,    73,    74,    74
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     9,     0,     3,     2,     0,     2,     1,
       4,     2,     0,     2,     1,     4,     1,     3,     5,     4,
       1,     1,     1,     3,     3,     3,     3,     3,     2,     3,
       3,     1,     7,     2,     7,     2,     0,     7,     2,     7,
       2,     7,     2,     3,     2,     1,     1,     1,     3,     3,
       2,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       2,     2,     2,     3,     3,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 109 "src/parse.y" /* yacc.c:1646  */
    {
                          replicate(out, (yyvsp[-7].node_ptr), (yyvsp[-6].node_ptr), (yyvsp[-5].node_ptr), (yyvsp[-4].node_ptr), (yyvsp[-3].node_ptr), (yyvsp[-2].node_ptr), (yyvsp[-1].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1444 "src/parse.c" /* yacc.c:1646  */
    break;

  case 4:
#line 115 "src/parse.y" /* yacc.c:1646  */
    {
                          names_and_types = true;
                        }
#line 1452 "src/parse.c" /* yacc.c:1646  */
    break;

  case 5:
#line 119 "src/parse.y" /* yacc.c:1646  */
    {
                          names_and_types = false;
                        }
#line 1460 "src/parse.c" /* yacc.c:1646  */
    break;

  case 10:
#line 133 "src/parse.y" /* yacc.c:1646  */
    {
                          char *key = (yyvsp[-3].string);
                          node *value = lin_expr_calc((yyvsp[-1].node_ptr));
                          lin_hashtable_insert(name_table, key, value);
                          LIN_FREE((yyvsp[-3].string));
                          free_node((yyvsp[-1].node_ptr));
                        }
#line 1472 "src/parse.c" /* yacc.c:1646  */
    break;

  case 13:
#line 147 "src/parse.y" /* yacc.c:1646  */
    {
                          node *res;
                          if (names_and_types) {
                            res = NULL;
                          }
                          else {
                            res = lin_add_node_to_list((yyvsp[-1].node_ptr), (yyvsp[0].node_ptr));
                          }
                          (yyval.node_ptr) = res;
                        }
#line 1487 "src/parse.c" /* yacc.c:1646  */
    break;

  case 14:
#line 158 "src/parse.y" /* yacc.c:1646  */
    { 
                          node *res = NULL;
                          if (!names_and_types) {
                            res = lin_new_list_node_n(1, (yyvsp[0].node_ptr));
                          }
                          (yyval.node_ptr) = res;
                        }
#line 1499 "src/parse.c" /* yacc.c:1646  */
    break;

  case 15:
#line 168 "src/parse.y" /* yacc.c:1646  */
    {
                          node *res;
                          char *type_name = (yyvsp[-3].string);
                          if (names_and_types) { /* type declaration */
                            lin_fatalerror(lin_hashtable_lookup(type_table, type_name, NULL), LIN_ERR_MSG_TYPE_REDECL, type_name, yylineno);
                            node *type = (yyvsp[-1].node_ptr), *type_real;
                            if (is_id(type)) { /* look up for type in type_table */
                              lin_fatalerror(!lin_hashtable_lookup(type_table, get_value(type), &type_real), LIN_ERR_MSG_UNKNOWN_TYPE, get_value(type), yylineno);
                              free_node(type);
                              type = copy_node(type_real);
                            }
                            lin_hashtable_insert(type_table, type_name, type);
                            res = NULL;
                            LIN_FREE(type_name);
                          }
                          else { /* variable declaration */
                            char *var_name = (yyvsp[-3].string);
                            lin_fatalerror(lin_hashtable_lookup(vars_table, var_name, NULL), LIN_ERR_MSG_VAR_REDECL, type_name, yylineno);
                            node *type = (yyvsp[-1].node_ptr), *type_real;
                            if (is_id(type)) {
                              lin_fatalerror(!lin_hashtable_lookup(type_table, get_value(type), &type_real), LIN_ERR_MSG_UNKNOWN_TYPE, get_value(type), yylineno);
                              free_node(type);
                              type = copy_node(type_real);
                            }
                            lin_hashtable_insert(vars_table, var_name, copy_node(type));
                            res = lin_new_list_node_n(2, new_id_node(var_name), type);
                          }
                          (yyval.node_ptr) = res;
                        }
#line 1533 "src/parse.c" /* yacc.c:1646  */
    break;

  case 16:
#line 199 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = new_id_node((yyvsp[0].string));
                        }
#line 1541 "src/parse.c" /* yacc.c:1646  */
    break;

  case 17:
#line 203 "src/parse.y" /* yacc.c:1646  */
    {
                          node *lb = lin_expr_calc((yyvsp[-2].node_ptr));
                          free_node((yyvsp[-2].node_ptr));
                          node *ub = lin_expr_calc((yyvsp[0].node_ptr));
                          free_node((yyvsp[0].node_ptr));
                          lin_fatalerror(!is_number(lb) || !is_number(ub), LIN_ERR_MSG_BOUNDS_MUST_BE_INTEGER, yylineno);
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_INT), lb, ub);
                        }
#line 1554 "src/parse.c" /* yacc.c:1646  */
    break;

  case 18:
#line 212 "src/parse.y" /* yacc.c:1646  */
    {
                          node *lb = lin_expr_calc((yyvsp[-3].node_ptr));
                          free_node((yyvsp[-3].node_ptr));
                          node *ub = lin_expr_calc((yyvsp[-1].node_ptr));
                          free_node((yyvsp[-1].node_ptr));
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_REAL), lb, ub);
                        }
#line 1566 "src/parse.c" /* yacc.c:1646  */
    break;

  case 19:
#line 220 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_add_node_to_list(lin_new_keyword_node(LIN_KEYWORD_ENUM), (yyvsp[-1].node_ptr));
                        }
#line 1574 "src/parse.c" /* yacc.c:1646  */
    break;

  case 20:
#line 226 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = new_number_node((yyvsp[0].string));
                        }
#line 1582 "src/parse.c" /* yacc.c:1646  */
    break;

  case 21:
#line 230 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = new_realnumber_node((yyvsp[0].string));
                        }
#line 1590 "src/parse.c" /* yacc.c:1646  */
    break;

  case 22:
#line 234 "src/parse.y" /* yacc.c:1646  */
    {
                          char *name = (yyvsp[0].string);
                          node *val, *res;
                          if (lin_hashtable_lookup(name_table, name, &val)) {
                            res = copy_node(val);
                          }
                          else {
                            if (!lin_get_var_type(name)) {
                              lin_fatalerror(1, LIN_ERR_MSG_UNKNOWN_SYMBOL, name, yylineno);
                            }
                            res = new_id_node(strdup(name));
                          }
                          LIN_FREE((yyvsp[0].string));
                          (yyval.node_ptr) = res;
                        }
#line 1610 "src/parse.c" /* yacc.c:1646  */
    break;

  case 23:
#line 250 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_PLUS), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1618 "src/parse.c" /* yacc.c:1646  */
    break;

  case 24:
#line 254 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_MINUS), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1626 "src/parse.c" /* yacc.c:1646  */
    break;

  case 25:
#line 258 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_TIMES), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1634 "src/parse.c" /* yacc.c:1646  */
    break;

  case 26:
#line 262 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_DIVIDE), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1642 "src/parse.c" /* yacc.c:1646  */
    break;

  case 27:
#line 266 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_MOD), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1650 "src/parse.c" /* yacc.c:1646  */
    break;

  case 28:
#line 270 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(2, lin_new_keyword_node(LIN_KEYWORD_MINUS), (yyvsp[0].node_ptr));
                        }
#line 1658 "src/parse.c" /* yacc.c:1646  */
    break;

  case 29:
#line 274 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[-1].node_ptr);
                        }
#line 1666 "src/parse.c" /* yacc.c:1646  */
    break;

  case 30:
#line 280 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_add_node_to_list((yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1674 "src/parse.c" /* yacc.c:1646  */
    break;

  case 31:
#line 284 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(1, (yyvsp[0].node_ptr));
                        }
#line 1682 "src/parse.c" /* yacc.c:1646  */
    break;

  case 32:
#line 290 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, (yyvsp[-4].node_ptr), (yyvsp[-2].node_ptr));
                        }
#line 1690 "src/parse.c" /* yacc.c:1646  */
    break;

  case 33:
#line 294 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[0].node_ptr);
                        }
#line 1698 "src/parse.c" /* yacc.c:1646  */
    break;

  case 34:
#line 300 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, (yyvsp[-4].node_ptr), (yyvsp[-2].node_ptr));
                        }
#line 1706 "src/parse.c" /* yacc.c:1646  */
    break;

  case 35:
#line 304 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[0].node_ptr);
                        }
#line 1714 "src/parse.c" /* yacc.c:1646  */
    break;

  case 36:
#line 308 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = NULL;
                        }
#line 1722 "src/parse.c" /* yacc.c:1646  */
    break;

  case 37:
#line 314 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, (yyvsp[-4].node_ptr), (yyvsp[-2].node_ptr));
                        }
#line 1730 "src/parse.c" /* yacc.c:1646  */
    break;

  case 38:
#line 318 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[0].node_ptr);
                        }
#line 1738 "src/parse.c" /* yacc.c:1646  */
    break;

  case 39:
#line 324 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, (yyvsp[-4].node_ptr), (yyvsp[-2].node_ptr));
                        }
#line 1746 "src/parse.c" /* yacc.c:1646  */
    break;

  case 40:
#line 328 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[0].node_ptr);
                        }
#line 1754 "src/parse.c" /* yacc.c:1646  */
    break;

  case 41:
#line 334 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, (yyvsp[-4].node_ptr), (yyvsp[-2].node_ptr));
                        }
#line 1762 "src/parse.c" /* yacc.c:1646  */
    break;

  case 42:
#line 338 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[0].node_ptr);
                        }
#line 1770 "src/parse.c" /* yacc.c:1646  */
    break;

  case 43:
#line 344 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1778 "src/parse.c" /* yacc.c:1646  */
    break;

  case 44:
#line 348 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[-1].node_ptr);
                        }
#line 1786 "src/parse.c" /* yacc.c:1646  */
    break;

  case 45:
#line 354 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_keyword_node(LIN_KEYWORD_TRUE);
                        }
#line 1794 "src/parse.c" /* yacc.c:1646  */
    break;

  case 46:
#line 358 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_keyword_node(LIN_KEYWORD_FALSE);
                        }
#line 1802 "src/parse.c" /* yacc.c:1646  */
    break;

  case 47:
#line 362 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_handle_constraint((yyvsp[0].node_ptr));
                        }
#line 1810 "src/parse.c" /* yacc.c:1646  */
    break;

  case 48:
#line 366 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[-1].node_ptr);
                        }
#line 1818 "src/parse.c" /* yacc.c:1646  */
    break;

  case 49:
#line 370 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[-1].node_ptr);
                        }
#line 1826 "src/parse.c" /* yacc.c:1646  */
    break;

  case 50:
#line 374 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(2, lin_new_keyword_node(LIN_KEYWORD_NOT), (yyvsp[0].node_ptr));
                        }
#line 1834 "src/parse.c" /* yacc.c:1646  */
    break;

  case 51:
#line 378 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1842 "src/parse.c" /* yacc.c:1646  */
    break;

  case 52:
#line 382 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_OR), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1850 "src/parse.c" /* yacc.c:1646  */
    break;

  case 53:
#line 386 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_XOR), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1858 "src/parse.c" /* yacc.c:1646  */
    break;

  case 54:
#line 390 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_IMPLY), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1866 "src/parse.c" /* yacc.c:1646  */
    break;

  case 55:
#line 396 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_GE), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1874 "src/parse.c" /* yacc.c:1646  */
    break;

  case 56:
#line 400 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_GT), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1882 "src/parse.c" /* yacc.c:1646  */
    break;

  case 57:
#line 404 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_LE), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1890 "src/parse.c" /* yacc.c:1646  */
    break;

  case 58:
#line 408 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_LT), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1898 "src/parse.c" /* yacc.c:1646  */
    break;

  case 59:
#line 412 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_EQ), (yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1906 "src/parse.c" /* yacc.c:1646  */
    break;

  case 60:
#line 418 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[0].node_ptr);
                        }
#line 1914 "src/parse.c" /* yacc.c:1646  */
    break;

  case 61:
#line 424 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[0].node_ptr);
                        }
#line 1922 "src/parse.c" /* yacc.c:1646  */
    break;

  case 62:
#line 430 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = (yyvsp[0].node_ptr);
                        }
#line 1930 "src/parse.c" /* yacc.c:1646  */
    break;

  case 63:
#line 436 "src/parse.y" /* yacc.c:1646  */
    {
                          char *var_name = (yyvsp[-2].string);
                          lin_fatalerror(lin_hashtable_lookup(vars_table, var_name, NULL), LIN_ERR_MSG_VAR_REDECL, var_name, yylineno); 
                          lin_fatalerror(lin_hashtable_lookup(local_vars_table, var_name, NULL), LIN_ERR_MSG_VAR_REDECL, var_name, yylineno); 
                          node *type = (yyvsp[0].node_ptr), *type_real;
                          if (is_id(type)) {
                            char *type_name = get_value(type);
                            lin_fatalerror(!lin_hashtable_lookup(type_table, type_name, &type_real), LIN_ERR_MSG_UNKNOWN_TYPE, type_name, yylineno);
                            free_node(type);
                            type = copy_node(type_real);
                          }
                          lin_hashtable_insert(local_vars_table, var_name, copy_node(type));
                          (yyval.node_ptr) = lin_new_list_node_n(2, new_id_node(var_name), type);
                        }
#line 1949 "src/parse.c" /* yacc.c:1646  */
    break;

  case 64:
#line 453 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_add_node_to_list((yyvsp[-2].node_ptr), (yyvsp[0].node_ptr));
                        }
#line 1957 "src/parse.c" /* yacc.c:1646  */
    break;

  case 65:
#line 457 "src/parse.y" /* yacc.c:1646  */
    {
                          (yyval.node_ptr) = lin_new_list_node_n(1, (yyvsp[0].node_ptr));
                        }
#line 1965 "src/parse.c" /* yacc.c:1646  */
    break;


#line 1969 "src/parse.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 462 "src/parse.y" /* yacc.c:1906  */


void
yyerror(char *s) {
  fprintf(stderr, "line %d: %s\n", yylineno, s);
}

void
lin_parse(FILE *f_in, FILE *f_out) {
  name_table = lin_hashtable_init(NULL, free_node); 
  type_table = lin_hashtable_init(NULL, free_node);
  vars_table = lin_hashtable_init(NULL, free_node);
  local_vars_table = lin_hashtable_init(NULL, free_node);
  exists_local_vars = NULL;

  yyin = f_in;
  out = f_out;
  lin_fatalerror(yyparse(), LIN_ERR_MSG_PARSING_FAILED);

  lin_hashtable_free(name_table);
  lin_hashtable_free(type_table);
  lin_hashtable_free(vars_table);
  lin_hashtable_free(local_vars_table);
}

node *
lin_handle_quantized_formula(Lin_keyword_id keyword_id, node *local_vars_list, node *formula_list) {
  node *res;
  res = formula_list;
  size_t i;
  size_t local_vars_num = get_children_num(local_vars_list);
  node *local_var;
  for (i = 0; i< local_vars_num; ++i) {
    local_var = get_ith_child(local_vars_list, i);
    lin_hashtable_remove(local_vars_table, get_value(get_ith_child(local_var, 0)));
  }
  return lin_new_list_node_n(3, lin_new_keyword_node(keyword_id), local_vars_list, res);
}

node *
lin_handle_constraint(node *constraint) {
  return constraint;
}

static node *
lin_get_var_type(const char *name) {
  node *type;
  char *nname = strdup(name);
  bool defined;
  defined = lin_hashtable_lookup(vars_table, name, &type);
  if (!defined) {
    defined = lin_hashtable_lookup(local_vars_table, name, &type);
  }
  size_t len = strlen(name);
  if (!defined && (name[len - 1] == '\'')) {
    nname[len - 1] = '\0';
    defined = lin_hashtable_lookup(vars_table, nname, &type);
  }
  LIN_FREE(nname);
  if (defined) {
    return type;
  }
  else {
    return NULL;
  }
}

static node *
lin_create_var(const char *name, Lin_keyword_id type, double lb, double ub) {
  node *node_name = new_id_node(strdup(name));
  node *node_lb = lin_new_realnumber_node(lb);
  node *node_ub = lin_new_realnumber_node(ub);
  node *node_type = lin_new_list_node_n(3, lin_new_keyword_node(type), node_lb, node_ub);
  return lin_new_list_node_n(2, node_name, node_type);
}

static node *
lin_handle_formula(node *formula) {
  node *res;
  if (exists_local_vars == NULL) {
    res = formula;
  }
  else {
    if (is_list(formula) && lin_is_keyword_node(get_ith_child(formula, 0), LIN_KEYWORD_EXISTS)) {
      node *local_vars = get_ith_child(formula, 1);
      size_t local_vars_num = get_children_num(local_vars);
      size_t exists_local_vars_num = get_children_num(exists_local_vars);
      size_t local_vars_all_num = local_vars_num + exists_local_vars_num;
      node **children;
      LIN_MALLOC(children, local_vars_all_num, node *);
      size_t i;
      for (i = 0; i < local_vars_num; ++i) {
        children[i] = copy_node(get_ith_child(local_vars, i));
      }
      free_node(local_vars);
      for (i = 0; i < exists_local_vars_num; ++i) {
        children[i + local_vars_num] = copy_node(get_ith_child(exists_local_vars, i));
      }
      free_node(exists_local_vars);
      node *local_vars_all = lin_new_list_node(local_vars_all_num, children);
      set_ith_child(formula, 1, local_vars_all);
      res = formula;
    }
    else {
      res = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_EXISTS), exists_local_vars, formula);
    }
  }
  exists_local_vars = NULL;
  return res;
}
