#include <stdbool.h>
#include <uthash.h>

#include "errors.h"
#include "memory.h"
#include "hashtable.h"

typedef struct Lin_hashtable_elem Lin_hashtable_elem;

struct Lin_hashtable {
  Lin_hashtable_elem *elems;
  Lin_hashtable_value_copy_f value_copy_f;
  Lin_hashtable_value_dtor_f value_dtor_f;
};

struct Lin_hashtable_elem {
  char *key;
  void *value;
  UT_hash_handle hh;
};

struct Lin_hashtable_iter {
  struct Lin_hashtable *table;
  Lin_hashtable_elem *elem;
};

Lin_hashtable *
lin_hashtable_init(Lin_hashtable_value_copy_f value_copy_f, Lin_hashtable_value_dtor_f value_dtor_f) {
  struct Lin_hashtable *table;
  LIN_MALLOC(table, 1, struct Lin_hashtable);
  table->elems = NULL;
  table->value_copy_f = value_copy_f;
  table->value_dtor_f = value_dtor_f;
  return (Lin_hashtable *) table;
}

void
lin_hashtable_free(Lin_hashtable *_table) {
  lin_hashtable_clear(_table);
  struct Lin_hashtable *table = (struct Lin_hashtable *) _table;
  LIN_FREE(table);
}

void
lin_hashtable_insert(Lin_hashtable *_table, const char *key, void *value) {
  struct Lin_hashtable *table = (struct Lin_hashtable *) _table;
  Lin_hashtable_elem *elem;
  HASH_FIND_STR(table->elems, key, elem);
  if (elem == NULL) {
    LIN_MALLOC(elem, 1, Lin_hashtable_elem);
    elem->key = strdup(key);
    HASH_ADD_KEYPTR(hh, table->elems, elem->key, strlen(elem->key), elem);
  }
  else {
    if (table->value_dtor_f != NULL) {
      table->value_dtor_f(elem->value);
    }
  }
  if (table->value_copy_f != NULL) {
    table->value_copy_f(elem->value, value);
  }
  else {
    elem->value = value;
  }
}

bool
lin_hashtable_lookup(Lin_hashtable *_table, const char *key, void **value) {
  struct Lin_hashtable *table = (struct Lin_hashtable *) _table;
  Lin_hashtable_elem *elem = NULL;
  HASH_FIND_STR(table->elems, key, elem);
  if (elem != NULL) {
    if (value != NULL) {
      *value = elem->value;
    }
    return true;
  }
  else {
    return false;
  }
}

bool
lin_hashtable_remove(Lin_hashtable *_table, const char *key) {
  struct Lin_hashtable *table = (struct Lin_hashtable *) _table;
  Lin_hashtable_elem *elem = NULL;
  HASH_FIND_STR(table->elems, key, elem);
  if (elem != NULL) {
    HASH_DEL(table->elems, elem);
    if (table->value_dtor_f != NULL) {
      table->value_dtor_f(elem->value);
    }
    LIN_FREE(elem->key);
    LIN_FREE(elem);
    return true;
  }
  else {
    return false;
  }
}

void
lin_hashtable_clear(Lin_hashtable *_table) {
  struct Lin_hashtable *table = (struct Lin_hashtable *) _table;
  Lin_hashtable_elem *elem, *tmp;
  HASH_ITER(hh, table->elems, elem, tmp) {
    HASH_DEL(table->elems, elem);
    if (table->value_dtor_f != NULL) {
      table->value_dtor_f(elem->value);
    }
    LIN_FREE(elem->key);
    LIN_FREE(elem);
  }
}

size_t
lin_hashtable_size(Lin_hashtable *_table) {
  struct Lin_hashtable *table = (struct Lin_hashtable *) _table;
  size_t size = HASH_COUNT(table->elems);
  return size;
}

Lin_hashtable_iter *
lin_hashtable_iter_init(Lin_hashtable *_table) {
  struct Lin_hashtable *table = (struct Lin_hashtable *) _table;
  struct Lin_hashtable_iter *iter;
  LIN_MALLOC(iter, 1, struct Lin_hashtable_iter);
  iter->table = table;
  iter->elem = table->elems;
  return (Lin_hashtable_iter *) iter;
}

void
lin_hashtable_iter_free(Lin_hashtable_iter *_iter) {
  struct Lin_hashtable_iter *iter = (struct Lin_hashtable_iter *) _iter;
  LIN_FREE(iter);
}

bool
lin_hashtable_iter_next(Lin_hashtable_iter *_iter, char **key, void **value) {
  struct Lin_hashtable_iter *iter = (struct Lin_hashtable_iter *) _iter;
  if (iter->elem != NULL) {
    *key = iter->elem->key;
    *value = iter->elem->value;
    iter->elem = iter->elem->hh.next;
    return true;
  }
  else {
    return false;
  }
}
