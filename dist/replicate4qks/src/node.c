#include <string.h>
#include "node.h"
#include "node_internal.h"

enum yytokentype {
  REALNUMBER = 260,
  NUMBER = 261,
  ID = 262,
  OP = 263
};

int dot_i; /* in order to sequentially enumerate DOT nodes */

node *new_realnumber_node(char *value) {
  return (node *)newnode(REALNUMBER, value, 0, (struct node **)0);
}

node *new_number_node(char *value) {
  return (node *)newnode(NUMBER, value, 0, (struct node **)0);
}

node *new_id_node(char *value) {
  return (node *)newnode(ID, value, 0, (struct node **)0);
}

node *new_list_node(char *value, int nchildren, node **children) {
  return (node *)newnode(OP, value, nchildren, (struct node **)children);
}

node *copy_node(node *t) {
  int i;
  struct node *tree = (struct node *)t;
  struct node *res = (struct node *) malloc(sizeof(struct node));
  res->type = tree->type;
  if (tree->type == REALNUMBER || tree->type == NUMBER || tree->type == ID)
    res->value = strdup(tree->value);
  else {
    res->value = tree->value;
    res->nchildren = tree->nchildren;
    res->children = (struct node **) calloc(res->nchildren, sizeof(struct node *));
    for (i = 0; i < tree->nchildren; i++)
      res->children[i] = (struct node *)copy_node((node *)tree->children[i]);
  }
  return (node *)res;
}

void free_node(node *t) {
  struct node *tree = (struct node *)t;
  if (tree->type == REALNUMBER || tree->type == NUMBER || tree->type == ID)
    free(tree->value);
  else {
    int i, n = tree->nchildren;
    for (i = 0; i < n; i++)
      free_node((node *)tree->children[i]);
    free(tree->children);
  }
  free(tree);
}

void free_parsing_result() {
  free_node(parsing_result);
}

int is_realnumber(node *t) {
  struct node *tree = (struct node *)t;
  return (tree->type == REALNUMBER);
}

int is_number(node *t) {
  struct node *tree = (struct node *)t;
  return (tree->type == NUMBER);
}

int is_id(node *t) {
  struct node *tree = (struct node *)t;
  return (tree->type == ID);
}

int is_list(node *t) {
  struct node *tree = (struct node *)t;
  return (tree->type == OP);
}

node *get_parsing_result() {
  return (node *)parsing_result;
}

void set_parsing_result(node *t) {
  parsing_result = (struct node *)t;
}

char *get_value(node *t) {
  struct node *tree = (struct node *)t;
  return tree->value;
}

void set_value(node *t, char *v) {
  struct node *tree = (struct node *)t;
  tree->value = v;
}

int get_children_num(node *t) {
  struct node *tree = (struct node *)t;
  return tree->nchildren;
}

void set_children_num(node *t, int n) {
  struct node *tree = (struct node *)t;
  tree->nchildren = n;
}

node **get_children(node *t) {
  struct node *tree = (struct node *)t;
  return (node **)tree->children;
}

void set_children(node *t, node **children) {
  struct node *tree = (struct node *)t;
  tree->children = (struct node **)children;
}

node *get_ith_child(node *t, int i) {
  struct node *tree = (struct node *)t;
  return (node *)tree->children[i];
}

void set_ith_child(node *t, int i, node *child) {
  struct node *tree = (struct node *)t;
  tree->children[i] = (struct node *)child;
}

struct node *newnode(int type, char *value, int nchildren, struct node **children) {
  struct node *result = (struct node *) malloc(sizeof(struct node));
  result->type = type;
  result->value = value;
  result->nchildren = nchildren;
  result->children = children;
  return (node *)result;
}

void print_tree_pretty(FILE *out, node *t) {
  struct node *tree = (struct node *)t;
  if (tree->type == REALNUMBER || tree->type == NUMBER || tree->type == ID) {
    fprintf(out, "%s", tree->value);
  }
  else if (tree->type == OP) {
    int i;
    if (tree->value == (char *)1) fprintf(out, "(");
    for (i = 0; i < tree->nchildren; i++) {
      fprintf(out, "%s", i == 0? "" : " ");
      print_tree_pretty(out, tree->children[i]);
    }
    if (tree->value == (char *)1) fprintf(out, ")");
  }
}

void print_tree(FILE *out, node *t) {
  struct node *tree = (struct node *)t;
  if (tree->type == REALNUMBER) {
    fprintf(out, "[REALNUMBER,%s,0,NULL]", tree->value);
  }
  else if (tree->type == NUMBER) {
    fprintf(out, "[NUMBER,%s,0,NULL]", tree->value);
  }
  else if (tree->type == ID) {
    fprintf(out, "[ID,%s,0,NULL]", tree->value);
  }
  else if (tree->type == OP) {
    int i;
    if (tree->value == (char *)1) fprintf(out, "(");
    fprintf(out, "[OP,%s]", tree->value == (char *)1? "(char *)1" : "(null)");
    for (i = 0; i < tree->nchildren; i++) {
      fprintf(out, " ");
      print_tree(out, tree->children[i]);
    }
    if (tree->value == (char *)1) fprintf(out, ")");
  }
}

void print_tree_dot(FILE *out, node *t) {
  struct node *tree = (struct node *)t;
  fprintf(out, "digraph tree {\n");
  dot_i = 0;
  print_tree_dot_rec(out, tree);
  fprintf(out, "}\n");
}

void print_tree_dot_pretty(FILE *out, node *t) {
  struct node *tree = (struct node *)t;
  if (tree == (struct node *)0) return;
  fprintf(out, "digraph tree_pretty {\n");
  dot_i = 0;
  print_tree_dot_pretty_rec(out, tree);
  fprintf(out, "}\n");
}


int print_tree_dot_rec(FILE *out, struct node *tree) {
  int me = dot_i++;
  int child;
  if (tree == (struct node *)0) {
    fprintf(out, "  n%d [label=NULL];\n", me);
  }
  else if ((tree->type == REALNUMBER) || (tree->type == NUMBER) || (tree->type == ID)) {
    char *str = (char *) calloc(20, sizeof(char));
    switch (tree->type) {
    case REALNUMBER:
      sprintf(str, "%s", "REALNUMBER");
      break;
    case NUMBER:
      sprintf(str, "%s", "NUMBER");
      break;
    case ID:
      sprintf(str, "%s", "ID");
      break;
    default:
      sprintf(str, "%s", "NONE");
    }
    fprintf(out, "  n%d [label=\"%s, %s\"];\n", me, str, tree->value);
    child = print_tree_dot_rec(out, (struct node *)0);
    fprintf(out, "  n%d -> n%d;\n", me, child);
    fflush(out); free(str);
  }
  else if (tree->type == OP) {
    int i;
    fprintf(out, "  n%d [label=\"OP, %s\"];\n", me, tree->value == (char *)1? "(char *)1" : "(null)");
    for (i = 0; i < tree->nchildren; i++) {
      child = print_tree_dot_rec(out, tree->children[i]);
      fprintf(out, "  n%d -> n%d;\n", me, child);
    }
    fflush(out);
  }
  return me;
}

int print_tree_dot_pretty_rec(FILE *out, struct node *tree) {
  int me = dot_i++;
  int child;
  if ((tree->type == REALNUMBER) || (tree->type == NUMBER) || (tree->type == ID)) {
    fprintf(out, "  n%d [label=\"%s\"];\n", me, tree->value);
    fflush(out);
  }
  else if (tree->type == OP) {
    int i;
    fprintf(out, "  n%d [label=\"OP, %s\"];\n", me, tree->value == (char *)1? "(char *)1" : "(null)");
    for (i = 0; i < tree->nchildren; i++) {
      child = print_tree_dot_pretty_rec(out, tree->children[i]);
      fprintf(out, "  n%d -> n%d;\n", me, child);
    }
    fflush(out);
  }
  return me;
}

