#ifndef BOUNDS_H_
#define BOUNDS_H_

void
replicate(FILE *out, node *statevars, node *inputvars, node *outputvars, node *trans, node *safety, node *ctreg, node *goal, node *obs);

#endif /* #ifndef BOUNDS_H_ */
