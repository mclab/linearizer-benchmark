#ifndef LIN_ERRORS_H_
#define LIN_ERRORS_H_

#define LIN_ERR_MSG_MEMORY_ALLOC "Cannot allocate memory."
#define LIN_ERR_MSG_FUNC_NOT_EXIST "There is no function \"%s\" in library."
#define LIN_ERR_MSG_OPT_UNKNOWN "The \"%s\" command line option is unknown."
#define LIN_ERR_MSG_OPT_REQ_VAL "The \"%s\" command line option requires value."
#define LIN_ERR_MSG_OPT_REQ_NUM_VAL "The \"%s\" command line option requires number as a value."
#define LIN_ERR_MSG_OPT_REQ_POS_INT_VAL "The \"%s\" command line option requires positive integer value."
#define LIN_ERR_MSG_FILE_CANNOT_OPEN "Cannot open file at path \"%s\"."
#define LIN_ERR_MSG_FILE_CANNOT_CREATE "Cannot create file at path \"%s\"."
#define LIN_ERR_MSG_LIB_COMPILE "Cannot compile wrappers library."
#define LIN_ERR_MSG_LIB_LOAD "Cannot load wrappers library."
#define LIN_ERR_MSG_EXPR_CALC_MOD_RATIONAL "MOD operation is not allowed for rationals."
#define LIN_ERR_MSG_UNKNOWN_TYPE "Unknown type \"%s\" at line %d."
#define LIN_ERR_MSG_UNKNOWN_SYMBOL "Unknown symbol \"%s\" at line %d."
#define LIN_ERR_MSG_VAR_REDECL "Variable \"%s\" is redeclared at line %d."
#define LIN_ERR_MSG_TYPE_REDECL "Type \"%s\" is redeclared at line %d."
#define LIN_ERR_MSG_BOUNDS_MUST_BE_INTEGER "Bounds of integer inteval must be integer at line %d."
#define LIN_ERR_MSG_FUNC_NOT_DEFINED "Function \"%s\" is not defined at line %d."
#define LIN_ERR_MSG_FUNC_WRONG_ARITY "Wrong number of arguments for function \"%s\" at line %d."
#define LIN_ERR_MSG_ARG_NOT_REAL "Function argument \"%s\" is not real at line %d."
#define LIN_ERR_MSG_FUNC_WRONG_PERIOD "Function period doesn't match its arity at line %d."
#define LIN_ERR_MSG_PARSING_FAILED "Parsing failed."
#define LIN_ERR_MSG_FUNC_VALUE_NOT_FINITE "Calculated function value is not finite."

extern void
lin_fatalerror(int /*condition*/, const char * /*fmt*/, ...);

#endif /* #ifndef LIN_ERRORS_H_ */
