#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "errors.h"

void
lin_fatalerror(int condition, const char *fmt, ...) {
  if (condition) {
    va_list args;
    va_start(args, fmt);
    fprintf(stderr, "Fatal error: ");
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);
    exit(EXIT_FAILURE);
  }
}
