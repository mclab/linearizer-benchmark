/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_SRC_PARSE_H_INCLUDED
# define YY_YY_SRC_PARSE_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    LBRACKET = 258,
    RBRACKET = 259,
    LBRACE = 260,
    RBRACE = 261,
    LPAREN = 262,
    RPAREN = 263,
    NUMBER = 264,
    REALNUMBER = 265,
    CONST = 266,
    TYPE = 267,
    STATEVARS = 268,
    INPUTVARS = 269,
    OUTPUTVARS = 270,
    TRANS = 271,
    SAFETY = 272,
    CONTROLLABLEREGION = 273,
    GOAL = 274,
    OBSERVATION = 275,
    FUNCTIONS = 276,
    FORALL = 277,
    EXISTS = 278,
    ENUM = 279,
    TRUE = 280,
    FALSE = 281,
    DOTDOT = 282,
    SEMICOLON = 283,
    COLON = 284,
    COMMA = 285,
    GEQ = 286,
    GT = 287,
    LEQ = 288,
    LT = 289,
    EQ = 290,
    ID = 291,
    IMPLY = 292,
    OR = 293,
    XOR = 294,
    AND = 295,
    NOT = 296,
    PLUS = 297,
    MINUS = 298,
    TIMES = 299,
    DIVIDE = 300,
    MOD = 301,
    UMINUS = 302
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 76 "src/parse.y" /* yacc.c:1909  */

  char *string;
  node *node_ptr;

#line 107 "src/parse.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SRC_PARSE_H_INCLUDED  */
