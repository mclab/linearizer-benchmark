#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "errors.h"
#include "memory.h"
#include "hashtable.h"
#include "opts.h"

typedef struct Lin_opts Lin_opts;

struct Lin_opts {
  char *input_file_path;
  char *output_file_path;
  char *bounds_file_path;
  unsigned int num;
  unsigned int verbosity;
};

static Lin_opts *lin_opts = NULL;

void
lin_opts_init() {
  LIN_CALLOC(lin_opts, 1, Lin_opts);
  lin_opts_set_input_file_path(LIN_OPTS_DEF_INPUT_FILE_PATH);
  lin_opts_set_output_file_path(LIN_OPTS_DEF_OUTPUT_FILE_PATH);
  lin_opts_set_bounds_file_path(LIN_OPTS_DEF_BOUNDS_FILE_PATH);
  lin_opts_set_num(LIN_OPTS_DEF_NUM);
  lin_opts_set_verbosity(LIN_OPTS_DEF_VERBOSITY);
}

void
lin_opts_free() {
  LIN_FREE(lin_opts->input_file_path);
  LIN_FREE(lin_opts->output_file_path);
  LIN_FREE(lin_opts->bounds_file_path);

  LIN_FREE(lin_opts);
}

void
lin_opts_print() {
  fprintf(stderr, "Current options:\n");
  fprintf(stderr, "\tinput file path:\t\t\t\t\t\t\"%s\"\n", lin_opts_get_input_file_path());
  fprintf(stderr, "\toutput file path:\t\t\t\t\t\t\"%s\"\n", lin_opts_get_output_file_path());
  fprintf(stderr, "\tbounds file path:\t\t\t\t\t\t\"%s\"\n", lin_opts_get_bounds_file_path());
  fprintf(stderr, "\tnumber:\t\t\t\t\t\t%u\n", lin_opts_get_num());
  fprintf(stderr, "\tverbosity level:\t\t\t\t\t\t%u\n", lin_opts_get_verbosity());
}

const char *
lin_opts_get_input_file_path() {
  return lin_opts->input_file_path;
}

void
lin_opts_set_input_file_path(const char *value) {
  LIN_FREE(lin_opts->input_file_path);
  lin_opts->input_file_path = value ? strdup(value) : NULL;
}

const char *
lin_opts_get_output_file_path() {
  return lin_opts->output_file_path;
}

void
lin_opts_set_output_file_path(const char *value) {
  LIN_FREE(lin_opts->output_file_path);
  lin_opts->output_file_path = value ? strdup(value) : NULL;
}

const char *
lin_opts_get_bounds_file_path() {
  return lin_opts->bounds_file_path;
}

void
lin_opts_set_bounds_file_path(const char *value) {
  LIN_FREE(lin_opts->bounds_file_path);
  lin_opts->bounds_file_path = value ? strdup(value) : NULL;
}

unsigned int
lin_opts_get_num() {
  return lin_opts->num;
}

void
lin_opts_set_num(unsigned int value) {
  lin_opts->num = value;
}

unsigned int
lin_opts_get_verbosity() {
  return lin_opts->verbosity;
}

void
lin_opts_set_verbosity(unsigned int value) {
  lin_opts->verbosity = value;
}
