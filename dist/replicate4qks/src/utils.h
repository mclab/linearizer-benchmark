#ifndef LIN_UTILS_H_
#define LIN_UTILS_H_

extern FILE *
lin_file_open(const char * /*path*/);

extern FILE *
lin_file_create(const char * /*path*/);

extern int
lin_sprintf_smart(char ** /*str*/, const char * /*fmt*/, ...);

extern double
lin_ceil_decimal(double /*x*/);

extern double
lin_time_ms_cpu();

#endif /* #ifndef LIN_UTILS_H_ */
