#ifndef LIN_NODE_HELPERS_H_
#define LIN_NODE_HELPERS_H_

extern node *
lin_new_list_node(size_t /*children_num*/, node ** /*children*/);

extern node *
lin_new_list_node_n(size_t /*num*/, ...);

extern node *
lin_new_keyword_node(Lin_keyword_id /*id*/);

extern bool
lin_is_keyword_node(node * /*n*/, Lin_keyword_id /*id*/);

extern node *
lin_add_node_to_list(node * /*n*/, node * /*list*/);

extern node *
lin_expr_calc(node * /*expr*/);

extern node *
lin_mpq_get_node(mpq_t /*q*/);

extern node *
lin_mpz_get_node(mpz_t /*z*/);

extern void
lin_mpq_set_node(mpq_t /*q*/, node * /*n*/);

extern void
lin_mpz_set_node(mpz_t /*z*/, node * /*n*/);

extern node *
lin_new_realnumber_node(double /*d*/);

extern char *
lin_node_to_str(node * /*n*/);

#endif /* #ifndef LIN_NODE_HELPERS_H_ */
