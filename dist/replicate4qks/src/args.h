#ifndef LIN_ARGS_H_
#define LIN_ARGS_H_

void
lin_args_parse_cmd_line(int /*argc*/, char ** /*argv*/);

void
lin_args_print_usage(const char * /*path*/);

#endif /* #ifndef LIN_ARGS_H_ */
