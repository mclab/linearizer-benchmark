#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include <gmp.h>

#include "errors.h"
#include "memory.h"
#include "keywords.h"
#include "node.h"
#include "node_helpers.h"
#include "utils.h"
#include "output.h"
#include "hashtable.h"
#include "opts.h"

static node *
read_bound(char *line, bool is_primed) {
  char *token;
  double lb;
  double ub;
  char *name;
  token = strsep(&line, " ");
  lb = strtod(token, NULL);
  token = strsep(&line, " "); // skip <=
  token = strsep(&line, " ");
  bool flag;
  if (token[0] == 'x') {
    flag = token[strlen(token) - 1] == '\'';
    if (is_primed == flag) {
      name = strdup(token + 2);
      if (flag) {
        name[strlen(name) - 1] = '\0';
      }
      token = strsep(&line, " "); // skip <=
      token = strsep(&line, " ");
      ub = strtod(token, NULL);
      return lin_new_list_node_n(2, new_id_node(name), lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_REAL), lin_new_realnumber_node(lb), lin_new_realnumber_node(ub)));
    }
    else {
      return NULL;
    }
  }
  else {
    return NULL;
  }
}

static node *
read_bounds(char const *fname, bool is_primed) {
  FILE *stream = fopen(fname, "r");
  assert(stream != NULL);
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  node *res = NULL;
  node *n;
  while ((read = getline(&line, &len, stream)) != -1) {
    n = read_bound(line, is_primed);
    if (n != NULL) {
      res = lin_add_node_to_list(n, res);
    }
  }
  LIN_FREE(line);
  fclose(stream);
  return res;
}

node *
var_add_index(node *n, unsigned idx) {
  char *name = get_value(n);
  char *name_with_idx;
  lin_sprintf_smart(&name_with_idx, "%s_%u", name, idx);
  return new_id_node(name_with_idx);
}

node *
vars_add_index(node *vars, unsigned idx) {
  node *res = copy_node(vars);
  node *node_name;
  char *name;
  char *name_with_idx;
  for (unsigned i = 0; i < get_children_num(res); ++i) {
    node_name = get_ith_child(get_ith_child(res, i), 0);
    name = get_value(node_name);
    lin_sprintf_smart(&name_with_idx, "%s_%u", name, idx);
    LIN_FREE(name);
    set_value(node_name, name_with_idx);
  }
  return res;
}

node *
formula_add_index(node *formula, Lin_hashtable *statevars, Lin_hashtable *statevars_next, Lin_hashtable *inputvars, Lin_hashtable *localvars, unsigned idx, unsigned k) {
  if (is_id(formula)) {
    if (lin_hashtable_lookup(statevars, get_value(formula), NULL)) {
      if (idx > 0) {
        return var_add_index(formula, idx);
      }
      else {
        return copy_node(formula);
      }
    }
    else if (lin_hashtable_lookup(statevars_next, get_value(formula), NULL)) {
      if (idx == k - 1) {
        return copy_node(formula);
      }
      else {
        char *name = get_value(formula);
        name[strlen(name) - 1] = '\0';
        char *name_with_idx;
        lin_sprintf_smart(&name_with_idx, "%s_%u", name, idx + 1);
        name[strlen(name)] = '\'';
        return new_id_node(name_with_idx);
      }
    }
    else if (lin_hashtable_lookup(inputvars, get_value(formula), NULL)) {
      return copy_node(formula);
    }
    else if (lin_hashtable_lookup(localvars, get_value(formula), NULL)) {
      return var_add_index(formula, idx);
    }
    else {
      return copy_node(formula);
    }
  }
  else if (is_number(formula) || is_realnumber(formula)){
    return copy_node(formula);
  }
  else {
    node **list;
    LIN_MALLOC(list, get_children_num(formula), node *);
    for (unsigned i = 0; i < get_children_num(formula); ++i) {
      list[i] = formula_add_index(get_ith_child(formula, i), statevars, statevars_next, inputvars, localvars, idx, k);
    }
    return lin_new_list_node(get_children_num(formula), list);
  }
}

Lin_hashtable *
build_table(node *list, bool prime) {
  Lin_hashtable *res = lin_hashtable_init(NULL, NULL);
  node *node_name;
  char *name;
  for (unsigned i = 0; i < get_children_num(list); ++i) {
    node_name = get_ith_child(get_ith_child(list, i), 0);
    name = get_value(node_name);
    if (prime) {
      char *name_primed;
      lin_sprintf_smart(&name_primed, "%s'", name);
      lin_hashtable_insert(res, name_primed, NULL);
      LIN_FREE(name_primed);
    }
    else {
      lin_hashtable_insert(res, name, NULL);
    }
  }
  return res;
}

node *
merge_lists(node *list1, node *list2) {
  unsigned n1 = get_children_num(list1);
  unsigned n2 = get_children_num(list2);
  node **children;
  LIN_MALLOC(children, n1 + n2, node *);
  for (unsigned i = 0; i < n1; ++i) {
    children[i] = copy_node(get_ith_child(list1, i));
  }
  free_node(list1);
  for (unsigned i = 0; i < n2; ++i) {
    children[n1 + i] = copy_node(get_ith_child(list2, i));
  }
  free_node(list2);
  return lin_new_list_node(n1 + n2, children);
}

void
write_bound_local(FILE *stream, node *n) {
  char *name = get_value(get_ith_child(n, 0));
  node *type = get_ith_child(n, 1);
  node *type_name = get_ith_child(type, 0);
  bool is_real = lin_is_keyword_node(type_name, LIN_KEYWORD_REAL);
  fprintf(stream, "%s <= %s_%s <= %s\n", get_value(get_ith_child(type, 1)), is_real ? "a" : "z", name, get_value(get_ith_child(type, 2)));
}

void
write_bound_state(FILE *stream, node *n) {
  char *name = get_value(get_ith_child(n, 0));
  node *type = get_ith_child(n, 1);
  fprintf(stream, "%s <= x_%s <= %s\n", get_value(get_ith_child(type, 1)), name, get_value(get_ith_child(type, 2)));
}

void
write_bound_state_next(FILE *stream, node *n) {
  char *name = get_value(get_ith_child(n, 0));
  node *type = get_ith_child(n, 1);
  fprintf(stream, "%s <= x_%s' <= %s\n", get_value(get_ith_child(type, 1)), name, get_value(get_ith_child(type, 2)));
}

void
write_bound_input(FILE *stream, node *n) {
  char *name = get_value(get_ith_child(n, 0));
  node *type = get_ith_child(n, 1);
  fprintf(stream, "%s <= w_%s <= %s\n", get_value(get_ith_child(type, 1)), name, get_value(get_ith_child(type, 2)));
}

void
write_bounds_file(node *statevars, node *statevars_next, node *inputvars, node *localvars) {
  FILE *stream = fopen(lin_opts_get_bounds_file_path(), "w");
  for (unsigned i = 0; i < get_children_num(statevars); ++i) {
    write_bound_state(stream, get_ith_child(statevars, i));
  }
  for (unsigned i = 0; i < get_children_num(statevars_next); ++i) {
    write_bound_state_next(stream, get_ith_child(statevars_next, i));
  }
  for (unsigned i = 0; i < get_children_num(inputvars); ++i) {
    write_bound_input(stream, get_ith_child(inputvars, i));
  }
  for (unsigned i = 0; i < get_children_num(localvars); ++i) {
    write_bound_local(stream, get_ith_child(localvars, i));
  }
  fclose(stream);
}

void
replicate(FILE *out, node *statevars, node *inputvars, node *outputvars, node *trans, node *safety, node *ctreg, node *goal, node *obs) {
  char *tmp_dir;
  lin_sprintf_smart(&tmp_dir, "%s/XXXXXX", P_tmpdir);
  mkdtemp(tmp_dir);
  char *model_fname;
  lin_sprintf_smart(&model_fname, "%s/model.m", tmp_dir);
  char *bounds_fname;
  lin_sprintf_smart(&bounds_fname, "%s/computed_bounds.txt", tmp_dir);
  char *cmdline;
  lin_sprintf_smart(&cmdline, "qks -preproc_only -d %s -dk %s 1>/dev/null 2>/dev/null", tmp_dir, tmp_dir);

  node *statevars_real_bounds;
  node *statevars_next = statevars;
  node *localvars = get_ith_child(trans, 1);
  node *formula = get_ith_child(trans, 2);
  node *localvars_replicated = copy_node(localvars);
  node *formula_replicated = formula;
  node *node_tmp;
  Lin_hashtable *statevars_table = build_table(statevars, false);
  Lin_hashtable *statevars_next_table = build_table(statevars, true);
  Lin_hashtable *inputvars_table = build_table(inputvars, false);
  Lin_hashtable *localvars_table = build_table(localvars, false);
  unsigned k = lin_opts_get_num();
  for (unsigned i = 0; i < k; ++i) {
    FILE *tmp_f = fopen(model_fname, "w");
    output_all(tmp_f, statevars_next, inputvars, outputvars, trans, safety, ctreg, goal, obs);
    fclose(tmp_f);
    system(cmdline);
    statevars_next = read_bounds(bounds_fname, true);
    if (i == 0) {
      statevars_real_bounds = read_bounds(bounds_fname, false);
    }
    if ((i < k - 1) && k > 1) {
      localvars_replicated = merge_lists(vars_add_index(localvars, i), localvars_replicated);
      localvars_replicated = merge_lists(vars_add_index(statevars_next, i), localvars_replicated);
      node_tmp = formula_add_index(formula, statevars_table, statevars_next_table, inputvars_table, localvars_table, i, k - 1);
      if (i == 0) {
        formula_replicated = node_tmp;
      }
      else {
        formula_replicated = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), node_tmp, formula_replicated);
      }
    }
  }

  LIN_FREE(cmdline);
  LIN_FREE(model_fname);
  LIN_FREE(bounds_fname);

  node *trans_replicated = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_EXISTS), localvars_replicated, formula_replicated);
  output_all(out, statevars, inputvars, outputvars, trans_replicated, safety, ctreg, goal, obs);
  write_bounds_file(statevars_real_bounds, statevars_next, inputvars, localvars_replicated);
  lin_sprintf_smart(&cmdline, "rm -rf %s", tmp_dir);
  LIN_FREE(tmp_dir);
  system(cmdline);
  LIN_FREE(cmdline);
}
