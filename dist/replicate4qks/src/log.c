#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "errors.h"
#include "memory.h"
#include "opts.h"
#include "log.h"

void
lin_log(unsigned int level, const char *fmt, ...) {
  if (lin_opts_get_verbosity() >= level) {
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    va_end(args);
  }
}
