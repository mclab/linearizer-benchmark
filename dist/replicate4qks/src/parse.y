%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include <lpsolver.h>
#include <linearizer.h>

#include <gmp.h>

#include <utarray.h>

#include "node.h"

#include "errors.h"
#include "memory.h"
#include "utils.h"
#include "hashtable.h"
#include "opts.h"
#include "keywords.h"
#include "node_helpers.h"
#include "output.h"
#include "bounds.h"

extern int
yylex();
extern int
yyparse();
extern void
yyerror(char * /*s*/);

static node *
lin_handle_quantized_formula(Lin_keyword_id /*keyword_id*/, node * /*local_vars_list*/, node * /*formula_list*/);

static node *
lin_handle_constraint(node * /*constraint*/);

static node *
lin_get_var_type(const char * /*name*/);

static node *
lin_create_var(const char * /*name*/, Lin_keyword_id /*type*/, double /*lb*/, double /*ub*/);

static node *
lin_handle_formula(node * /*formula*/);

extern int yylineno;
extern FILE *yyin;

static Lin_hashtable *name_table;
static Lin_hashtable *type_table;
static Lin_hashtable *vars_table;
static Lin_hashtable *local_vars_table;

static bool names_and_types;
static FILE *out;
static node *exists_local_vars;
%}

%start starting
%token LBRACKET RBRACKET LBRACE RBRACE LPAREN RPAREN NUMBER REALNUMBER CONST TYPE STATEVARS INPUTVARS OUTPUTVARS TRANS SAFETY CONTROLLABLEREGION GOAL OBSERVATION FUNCTIONS FORALL EXISTS ENUM TRUE FALSE DOTDOT SEMICOLON COLON COMMA GEQ GT LEQ LT EQ ID

%nonassoc IMPLY
%left OR XOR
%left AND
%left NOT

%left PLUS MINUS
%left TIMES DIVIDE MOD
%left UMINUS
%union {
  char *string;
  node *node_ptr;
}

%type <node_ptr> model
%type <node_ptr> const_decl_atom;
%type <node_ptr> const_decl_list;
%type <node_ptr> type_decl_atom
%type <node_ptr> type_decl_list
%type <node_ptr> statevars_decl
%type <node_ptr> inputvars_decl
%type <node_ptr> outputvars_decl
%type <node_ptr> type
%type <node_ptr> trans_spec
%type <node_ptr> safety_spec
%type <node_ptr> controlregion_spec
%type <node_ptr> goal_spec
%type <node_ptr> observation_spec
%type <node_ptr> formula_list
%type <node_ptr> formula
%type <node_ptr> local_var
%type <node_ptr> local_vars_list
%type <node_ptr> expr
%type <node_ptr> expr_list
%type <node_ptr> linear_constraint

%%

starting                : model
                        ;

model                   : const_and_types statevars_decl inputvars_decl outputvars_decl trans_spec safety_spec controlregion_spec goal_spec observation_spec
                        {
                          replicate(out, $2, $3, $4, $5, $6, $7, $8, $9);
                        }
                        ;

const_and_types         :
                        {
                          names_and_types = true;
                        }
                          const_decl type_decl
                        {
                          names_and_types = false;
                        }
                        ;

const_decl              : CONST const_decl_list
                        | /* empty */
                        ;

const_decl_list         : const_decl_atom const_decl_list
                        | const_decl_atom
                        ;

const_decl_atom         : ID COLON expr SEMICOLON
                        {
                          char *key = $<string>1;
                          node *value = lin_expr_calc($<node_ptr>3);
                          lin_hashtable_insert(name_table, key, value);
                          LIN_FREE($<string>1);
                          free_node($<node_ptr>3);
                        }
                        ;

type_decl               : TYPE type_decl_list
                        | /* empty */
                        ;

type_decl_list          : type_decl_atom type_decl_list
                        {
                          node *res;
                          if (names_and_types) {
                            res = NULL;
                          }
                          else {
                            res = lin_add_node_to_list($1, $2);
                          }
                          $$ = res;
                        }
                        | type_decl_atom
                        { 
                          node *res = NULL;
                          if (!names_and_types) {
                            res = lin_new_list_node_n(1, $<node_ptr>1);
                          }
                          $$ = res;
                        }
                        ;

type_decl_atom          : ID COLON type SEMICOLON
                        {
                          node *res;
                          char *type_name = $<string>1;
                          if (names_and_types) { /* type declaration */
                            lin_fatalerror(lin_hashtable_lookup(type_table, type_name, NULL), LIN_ERR_MSG_TYPE_REDECL, type_name, yylineno);
                            node *type = $3, *type_real;
                            if (is_id(type)) { /* look up for type in type_table */
                              lin_fatalerror(!lin_hashtable_lookup(type_table, get_value(type), &type_real), LIN_ERR_MSG_UNKNOWN_TYPE, get_value(type), yylineno);
                              free_node(type);
                              type = copy_node(type_real);
                            }
                            lin_hashtable_insert(type_table, type_name, type);
                            res = NULL;
                            LIN_FREE(type_name);
                          }
                          else { /* variable declaration */
                            char *var_name = $<string>1;
                            lin_fatalerror(lin_hashtable_lookup(vars_table, var_name, NULL), LIN_ERR_MSG_VAR_REDECL, type_name, yylineno);
                            node *type = $3, *type_real;
                            if (is_id(type)) {
                              lin_fatalerror(!lin_hashtable_lookup(type_table, get_value(type), &type_real), LIN_ERR_MSG_UNKNOWN_TYPE, get_value(type), yylineno);
                              free_node(type);
                              type = copy_node(type_real);
                            }
                            lin_hashtable_insert(vars_table, var_name, copy_node(type));
                            res = lin_new_list_node_n(2, new_id_node(var_name), type);
                          }
                          $$ = res;
                        }

type                    : ID
                        {
                          $$ = new_id_node($<string>1);
                        }
                        | expr DOTDOT expr
                        {
                          node *lb = lin_expr_calc($1);
                          free_node($1);
                          node *ub = lin_expr_calc($3);
                          free_node($3);
                          lin_fatalerror(!is_number(lb) || !is_number(ub), LIN_ERR_MSG_BOUNDS_MUST_BE_INTEGER, yylineno);
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_INT), lb, ub);
                        }
                        | LBRACKET expr COMMA expr RBRACKET
                        {
                          node *lb = lin_expr_calc($2);
                          free_node($2);
                          node *ub = lin_expr_calc($4);
                          free_node($4);
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_REAL), lb, ub);
                        }
                        | ENUM LPAREN expr_list RPAREN
                        {
                          $$ = lin_add_node_to_list(lin_new_keyword_node(LIN_KEYWORD_ENUM), $3);
                        }
                        ;

expr                    : NUMBER
                        {
                          $$ = new_number_node($<string>1);
                        }
                        | REALNUMBER
                        {
                          $$ = new_realnumber_node($<string>1);
                        }
                        | ID
                        {
                          char *name = $<string>1;
                          node *val, *res;
                          if (lin_hashtable_lookup(name_table, name, &val)) {
                            res = copy_node(val);
                          }
                          else {
                            if (!lin_get_var_type(name)) {
                              lin_fatalerror(1, LIN_ERR_MSG_UNKNOWN_SYMBOL, name, yylineno);
                            }
                            res = new_id_node(strdup(name));
                          }
                          LIN_FREE($<string>1);
                          $$ = res;
                        }
                        | expr PLUS expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_PLUS), $1, $3);
                        }
                        | expr MINUS expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_MINUS), $1, $3);
                        }
                        | expr TIMES expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_TIMES), $1, $3);
                        }
                        | expr DIVIDE expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_DIVIDE), $1, $3);
                        }
                        | expr MOD expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_MOD), $1, $3);
                        }
                        | MINUS expr %prec UMINUS
                        {
                          $$ = lin_new_list_node_n(2, lin_new_keyword_node(LIN_KEYWORD_MINUS), $2);
                        }
                        | LPAREN expr RPAREN
                        {
                          $$ = $<node_ptr>2;
                        }
                        ;

expr_list               : expr COMMA expr_list
                        {
                          $$ = lin_add_node_to_list($1, $3);
                        }
                        | expr
                        {
                          $$ = lin_new_list_node_n(1, $1);
                        }
                        ;

trans_spec              : TRANS EXISTS local_vars_list LBRACE formula_list RBRACE SEMICOLON
                        {
                          $$ = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, $3, $5);
                        }
                        | TRANS formula_list
                        {
                          $$ = $2;
                        }
                        ;

safety_spec             : SAFETY EXISTS local_vars_list LBRACE formula_list RBRACE SEMICOLON
                        {
                          $$ = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, $3, $5);
                        }
                        | SAFETY formula_list
                        {
                          $$ = $2;
                        }
                        | /* empty */
                        {
                          $$ = NULL;
                        }
                        ;

controlregion_spec      : CONTROLLABLEREGION EXISTS local_vars_list LBRACE formula_list RBRACE SEMICOLON
                        {
                          $$ = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, $3, $5);
                        }
                        | CONTROLLABLEREGION formula_list
                        {
                          $$ = $2;
                        }
                        ;

goal_spec               : GOAL EXISTS local_vars_list LBRACE formula_list RBRACE SEMICOLON
                        {
                          $$ = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, $3, $5);
                        }
                        | GOAL formula_list
                        {
                          $$ = $2;
                        }
                        ;

observation_spec        : OBSERVATION EXISTS local_vars_list LBRACE formula_list RBRACE SEMICOLON
                        {
                          $$ = lin_handle_quantized_formula(LIN_KEYWORD_EXISTS, $3, $5);
                        }
                        | GOAL formula_list
                        {
                          $$ = $2;
                        }
                        ;

formula_list            : formula SEMICOLON formula_list
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), $1, $3);
                        }
                        | formula SEMICOLON
                        {
                          $$ = $1;
                        }
                        ;

formula                 : TRUE
                        {
                          $$ = lin_new_keyword_node(LIN_KEYWORD_TRUE);
                        }
                        | FALSE
                        {
                          $$ = lin_new_keyword_node(LIN_KEYWORD_FALSE);
                        }
                        | linear_constraint
                        {
                          $$ = lin_handle_constraint($1);
                        }
                        | LBRACE formula RBRACE
                        {
                          $$ = $2;
                        }
                        | LPAREN formula RPAREN
                        {
                          $$ = $2;
                        }
                        | NOT formula
                        {
                          $$ = lin_new_list_node_n(2, lin_new_keyword_node(LIN_KEYWORD_NOT), $2);
                        }
                        | formula AND formula
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), $1, $3);
                        }
                        | formula OR formula
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_OR), $1, $3);
                        }
                        | formula XOR formula
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_XOR), $1, $3);
                        }
                        | formula IMPLY formula
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_IMPLY), $1, $3);
                        }
                        ;

linear_constraint       : expr GEQ expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_GE), $1, $3);
                        }
                        | expr GT expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_GT), $1, $3);
                        }
                        | expr LEQ expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_LE), $1, $3);
                        }
                        | expr LT expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_LT), $1, $3);
                        }
                        | expr EQ expr
                        {
                          $$ = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_EQ), $1, $3);
                        }
                        ;

statevars_decl          : STATEVARS type_decl_list
                        {
                          $$ = $2;
                        }
                        ;

inputvars_decl          : INPUTVARS type_decl_list
                        {
                          $$ = $2;
                        }
                        ;

outputvars_decl         : OUTPUTVARS type_decl_list
                        {
                          $$ = $2;
                        }
                        ;

local_var               : ID COLON type
                        {
                          char *var_name = $<string>1;
                          lin_fatalerror(lin_hashtable_lookup(vars_table, var_name, NULL), LIN_ERR_MSG_VAR_REDECL, var_name, yylineno); 
                          lin_fatalerror(lin_hashtable_lookup(local_vars_table, var_name, NULL), LIN_ERR_MSG_VAR_REDECL, var_name, yylineno); 
                          node *type = $3, *type_real;
                          if (is_id(type)) {
                            char *type_name = get_value(type);
                            lin_fatalerror(!lin_hashtable_lookup(type_table, type_name, &type_real), LIN_ERR_MSG_UNKNOWN_TYPE, type_name, yylineno);
                            free_node(type);
                            type = copy_node(type_real);
                          }
                          lin_hashtable_insert(local_vars_table, var_name, copy_node(type));
                          $$ = lin_new_list_node_n(2, new_id_node(var_name), type);
                        }
                        ;

local_vars_list         : local_var COMMA local_vars_list
                        {
                          $$ = lin_add_node_to_list($1, $3);
                        }
                        | local_var
                        {
                          $$ = lin_new_list_node_n(1, $1);
                        }
                        ;

%%

void
yyerror(char *s) {
  fprintf(stderr, "line %d: %s\n", yylineno, s);
}

void
lin_parse(FILE *f_in, FILE *f_out) {
  name_table = lin_hashtable_init(NULL, free_node); 
  type_table = lin_hashtable_init(NULL, free_node);
  vars_table = lin_hashtable_init(NULL, free_node);
  local_vars_table = lin_hashtable_init(NULL, free_node);
  exists_local_vars = NULL;

  yyin = f_in;
  out = f_out;
  lin_fatalerror(yyparse(), LIN_ERR_MSG_PARSING_FAILED);

  lin_hashtable_free(name_table);
  lin_hashtable_free(type_table);
  lin_hashtable_free(vars_table);
  lin_hashtable_free(local_vars_table);
}

node *
lin_handle_quantized_formula(Lin_keyword_id keyword_id, node *local_vars_list, node *formula_list) {
  node *res;
  res = formula_list;
  size_t i;
  size_t local_vars_num = get_children_num(local_vars_list);
  node *local_var;
  for (i = 0; i< local_vars_num; ++i) {
    local_var = get_ith_child(local_vars_list, i);
    lin_hashtable_remove(local_vars_table, get_value(get_ith_child(local_var, 0)));
  }
  return lin_new_list_node_n(3, lin_new_keyword_node(keyword_id), local_vars_list, res);
}

node *
lin_handle_constraint(node *constraint) {
  return constraint;
}

static node *
lin_get_var_type(const char *name) {
  node *type;
  char *nname = strdup(name);
  bool defined;
  defined = lin_hashtable_lookup(vars_table, name, &type);
  if (!defined) {
    defined = lin_hashtable_lookup(local_vars_table, name, &type);
  }
  size_t len = strlen(name);
  if (!defined && (name[len - 1] == '\'')) {
    nname[len - 1] = '\0';
    defined = lin_hashtable_lookup(vars_table, nname, &type);
  }
  LIN_FREE(nname);
  if (defined) {
    return type;
  }
  else {
    return NULL;
  }
}

static node *
lin_create_var(const char *name, Lin_keyword_id type, double lb, double ub) {
  node *node_name = new_id_node(strdup(name));
  node *node_lb = lin_new_realnumber_node(lb);
  node *node_ub = lin_new_realnumber_node(ub);
  node *node_type = lin_new_list_node_n(3, lin_new_keyword_node(type), node_lb, node_ub);
  return lin_new_list_node_n(2, node_name, node_type);
}

static node *
lin_handle_formula(node *formula) {
  node *res;
  if (exists_local_vars == NULL) {
    res = formula;
  }
  else {
    if (is_list(formula) && lin_is_keyword_node(get_ith_child(formula, 0), LIN_KEYWORD_EXISTS)) {
      node *local_vars = get_ith_child(formula, 1);
      size_t local_vars_num = get_children_num(local_vars);
      size_t exists_local_vars_num = get_children_num(exists_local_vars);
      size_t local_vars_all_num = local_vars_num + exists_local_vars_num;
      node **children;
      LIN_MALLOC(children, local_vars_all_num, node *);
      size_t i;
      for (i = 0; i < local_vars_num; ++i) {
        children[i] = copy_node(get_ith_child(local_vars, i));
      }
      free_node(local_vars);
      for (i = 0; i < exists_local_vars_num; ++i) {
        children[i + local_vars_num] = copy_node(get_ith_child(exists_local_vars, i));
      }
      free_node(exists_local_vars);
      node *local_vars_all = lin_new_list_node(local_vars_all_num, children);
      set_ith_child(formula, 1, local_vars_all);
      res = formula;
    }
    else {
      res = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_EXISTS), exists_local_vars, formula);
    }
  }
  exists_local_vars = NULL;
  return res;
}
