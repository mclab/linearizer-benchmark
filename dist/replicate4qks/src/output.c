#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <gmp.h>

#include "node.h"

#include "keywords.h"
#include "node_helpers.h"
#include "output.h"

const char *shift_str = "  ";

static void
lin_output_var(FILE * /*stream*/, node * /*var*/);

static void
lin_output_type(FILE * /*stream*/, node * /*type*/);

static void
lin_output_expr(FILE * /*stream*/, node * /*expr*/);

static void
lin_output_local_vars_list(FILE * /*stream*/, node * /*expr*/, size_t shift_num);

static bool
lin_output_expr_need_brackets(node * /*expr*/, node * /*op*/);

static bool
lin_output_formula_need_brackets(node * /*expr*/, node * /*op*/);

static void
lin_output_shift(FILE * /*stream*/, size_t /*shift_num*/);

void
lin_output_vars_list(FILE *stream, node *vars_list) {
  size_t i;
  size_t children_num = get_children_num(vars_list);
  node **children = get_children(vars_list);
  for (i = 0; i < children_num; ++i) {
    fprintf(stream, "%s", shift_str);
    lin_output_var(stream, children[i]);
    fprintf(stream, ";\n");
  }
}

void
lin_output_formula(FILE *stream, node *formula, size_t shift_num) {
  if (is_id(formula)) {
    lin_output_shift(stream, shift_num);
    fprintf(stream, "%s", get_value(formula));
  }
  else if (is_list(formula)) {
    size_t children_num = get_children_num(formula);
    node **children = get_children(formula);
    if (children_num == 3) {
      if (lin_is_keyword_node(children[0], LIN_KEYWORD_AND) ||
          lin_is_keyword_node(children[0], LIN_KEYWORD_OR) ||
          lin_is_keyword_node(children[0], LIN_KEYWORD_XOR) ||
          lin_is_keyword_node(children[0], LIN_KEYWORD_IMPLY)) {
        if (lin_output_formula_need_brackets(children[1], children[0])) {
          lin_output_shift(stream, shift_num);
          fprintf(stream, "(\n");
          lin_output_formula(stream, children[1], shift_num + 1);
          fprintf(stream, "\n");
          lin_output_shift(stream, shift_num);
          fprintf(stream, ")");
        }
        else {
          lin_output_formula(stream, children[1], shift_num);
        }
        fprintf(stream, " %s\n", get_value(children[0]));
        if (lin_output_formula_need_brackets(children[2], children[0])) {
          lin_output_shift(stream, shift_num);
          fprintf(stream, "(\n");
          lin_output_formula(stream, children[2], shift_num + 1);
          fprintf(stream, "\n");
          lin_output_shift(stream, shift_num);
          fprintf(stream, ")");
        }
        else {
          lin_output_formula(stream, children[2], shift_num);
        }
      }
      else if (lin_is_keyword_node(children[0], LIN_KEYWORD_FORALL) ||
               lin_is_keyword_node(children[0], LIN_KEYWORD_EXISTS)) {
        lin_output_shift(stream, shift_num);
        fprintf(stream, "%s\n", get_value(children[0]));
        lin_output_local_vars_list(stream, children[1], shift_num + 1);
        fprintf(stream, "\n");
        lin_output_shift(stream, shift_num);
        fprintf(stream, "{\n");
        lin_output_formula(stream, children[2], shift_num + 1);
        fprintf(stream, ";\n");
        lin_output_shift(stream, shift_num);
        fprintf(stream, "}");
      }
      else if (lin_is_keyword_node(children[0], LIN_KEYWORD_EQ) ||
               lin_is_keyword_node(children[0], LIN_KEYWORD_LE) ||
               lin_is_keyword_node(children[0], LIN_KEYWORD_LT) ||
               lin_is_keyword_node(children[0], LIN_KEYWORD_GE) ||
               lin_is_keyword_node(children[0], LIN_KEYWORD_GT) ||
               lin_is_keyword_node(children[0], LIN_KEYWORD_NEQ)) {
        lin_output_shift(stream, shift_num);
        fprintf(stream, "(");
        lin_output_expr(stream, children[1]);
        fprintf(stream, " %s ", get_value(children[0]));
        lin_output_expr(stream, children[2]);
        fprintf(stream, ")");
      }
    }
    else if ((children_num == 2) && (lin_is_keyword_node(children[0], LIN_KEYWORD_NOT))) {
      fprintf(stream, "(");
      fprintf(stream, "%s ", get_value(children[0]));
      lin_output_formula(stream, children[1], 0);
      fprintf(stream, ")");
    }
  }
}

static void
lin_output_expr(FILE *stream, node *expr) {
  if (is_id(expr) || is_number(expr) || is_realnumber(expr)) {
    fprintf(stream, "%s", get_value(expr));
  }
  else {
    size_t children_num = get_children_num(expr);
    node **children = get_children(expr);
    if (children_num == 3) {
      if (lin_output_expr_need_brackets(children[1], children[0])) {
        fprintf(stream, "(");
        lin_output_expr(stream, children[1]);
        fprintf(stream, ")");
      }
      else {
        lin_output_expr(stream, children[1]);
      }
      fprintf(stream, " %s ", get_value(children[0]));
      if (lin_output_expr_need_brackets(children[2], children[0])) {
        fprintf(stream, "(");
        lin_output_expr(stream, children[2]);
        fprintf(stream, ")");
      }
      else {
        lin_output_expr(stream, children[2]);
      }
    }
    else if ((children_num == 2) && (lin_is_keyword_node(children[0], LIN_KEYWORD_MINUS))) {
      fprintf(stream, "-");
      lin_output_expr(stream, children[1]);
    }
  }
}

static void
lin_output_shift(FILE *stream, size_t shift_num) {
  size_t i;
  for (i = 0; i < shift_num; ++i) {
    fprintf(stream, "%s", shift_str);
  }
}

static void
lin_output_var(FILE *stream, node *var) {
  node **children = get_children(var);
  node *name = children[0];
  node *type = children[1];
  fprintf(stream, "%s: ", get_value(name));
  lin_output_type(stream, type);
}

static void
lin_output_type(FILE *stream, node *type) {
  node **children = get_children(type);
  node *lb = children[1];
  node *ub = children[2];
  if (lin_is_keyword_node(children[0], LIN_KEYWORD_REAL)) {
    fprintf(stream, "[");
    lin_output_expr(stream, lb);
    fprintf(stream, ", ");
    lin_output_expr(stream, ub);
    fprintf(stream, "]");
  }
  else if (lin_is_keyword_node(children[0], LIN_KEYWORD_INT)) {
    lin_output_expr(stream, lb);
    fprintf(stream, " .. ");
    lin_output_expr(stream, ub);
  }
}

static void
lin_output_local_vars_list(FILE *stream, node *vars_list, size_t shift_num) {
  size_t i;
  size_t children_num = get_children_num(vars_list);
  node **children = get_children(vars_list);
  for (i = 0; i < children_num; ++i) {
    lin_output_shift(stream, shift_num);
    lin_output_var(stream, children[i]);
    if (i < children_num - 1) {
      fprintf(stream, ",\n");
    }
  }
}

static bool
lin_output_expr_need_brackets(node *expr, node *op) {
  if (is_id(expr) || is_number(expr) || is_realnumber(expr)) {
    return false;
  }
  else {
    size_t children_num = get_children_num(expr);
    node **children = get_children(expr);
    if (children_num == 3) {
      if (!lin_is_keyword_node(op, LIN_KEYWORD_PLUS) && !lin_is_keyword_node(op, LIN_KEYWORD_TIMES)) {
        return true;
      }
      else if (strcmp(get_value(op), get_value(children[0])) != 0) {
        if (lin_is_keyword_node(op, LIN_KEYWORD_PLUS)) {
          return false;
        }
        else {
          return true;
        }
      }
      else {
        return false;
      }
    }
    else {
      return true;
    }
  }
}

static bool
lin_output_formula_need_brackets(node *expr, node *op) {
  if (is_id(expr)) {
    /* true or false */
    return false;
  }
  else {
    size_t children_num = get_children_num(expr);
    node **children = get_children(expr);
    if (children_num == 3) {
      if (lin_is_keyword_node(children[0], LIN_KEYWORD_EQ) ||
          lin_is_keyword_node(children[0], LIN_KEYWORD_LE) ||
          lin_is_keyword_node(children[0], LIN_KEYWORD_LT) ||
          lin_is_keyword_node(children[0], LIN_KEYWORD_GE) ||
          lin_is_keyword_node(children[0], LIN_KEYWORD_GT) ||
          lin_is_keyword_node(children[0], LIN_KEYWORD_NEQ)) {
        /* linear constraint */
        return false;
      }
      else if (lin_is_keyword_node(op, LIN_KEYWORD_AND) || lin_is_keyword_node(op, LIN_KEYWORD_OR) || lin_is_keyword_node(op, LIN_KEYWORD_XOR)) {
        /* previous operator is associative */
        if (strcmp(get_value(op), get_value(children[0])) != 0) {
          /* current and previos operators differ */
          return true;
        }
        else {
          /* current and previos operators match */
          return false;
        }
      }
      else {
        /* previous operator is not associative*/
        return true;
      }
    }
    else {
      /* unary operator always need brackets */
      return true;
    }
  }
}

void
output_all(FILE *out, node *statevars, node *inputvars, node *outputvars, node *trans, node *safety, node *ctreg, node *goal, node *obs) {
  fprintf(out, "Statevars\n");
  lin_output_vars_list(out, statevars);
  fprintf(out, "\n");
  fprintf(out, "Inputvars\n");
  lin_output_vars_list(out, inputvars);
  fprintf(out, "\n");
  fprintf(out, "Outputvars\n");
  lin_output_vars_list(out, outputvars);
  fprintf(out, "\n");
  fprintf(out, "Trans\n");
  lin_output_formula(out, trans, 1);
  fprintf(out, ";\n\n");
  if (safety != NULL) {
    fprintf(out, "Safety\n");
    lin_output_formula(out, safety, 1);
    fprintf(out, ";\n\n");
  }
  fprintf(out, "ControllableRegion\n");
  lin_output_formula(out, ctreg, 1);
  fprintf(out, ";\n\n");
  fprintf(out, "Goal\n");
  lin_output_formula(out, goal, 1);
  fprintf(out, ";\n\n");
  fprintf(out, "Observation\n");
  lin_output_formula(out, obs, 1);
  fprintf(out, ";");
}
