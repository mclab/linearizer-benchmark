#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include <gmp.h>

#include <utarray.h>

#include "node.h"

#include "errors.h"
#include "memory.h"
#include "hashtable.h"
#include "opts.h"
#include "args.h"
#include "parse.h"
#include "utils.h"

extern void
lin_parse(FILE * /*f_in*/, FILE * /*f_out*/);

int
main(int argc, char *argv[]) {
  lin_opts_init();
  lin_args_parse_cmd_line(argc, argv);
  if (lin_opts_get_verbosity() > 0) {
    lin_opts_print();
  }

  char const *input_file_path = lin_opts_get_input_file_path();
  FILE *f_in = stdin;
  if (input_file_path != NULL) {
    f_in = lin_file_open(input_file_path);
  }
  char const *output_file_path = lin_opts_get_output_file_path();
  FILE *f_out = stdout;
  if (output_file_path != NULL) {
    f_out = lin_file_create(output_file_path);
  }
  lin_parse(f_in, f_out);
  if (input_file_path != NULL) {
    fclose(f_in);
  }
  if (output_file_path != NULL) {
    fclose(f_out);
  }

  lin_opts_free();

  exit(EXIT_SUCCESS);
}
